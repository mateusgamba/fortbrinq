<?php

use Illuminate\Http\Request;

Route::get('categories', 'CategoryController@index');
Route::get('categories/{slugCategory}', 'CategoryController@showBySlug');

Route::group([ 'prefix' => 'auth' ], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');

    Route::get('unauthenticated', 'AuthController@unauthenticated')->name('login');

    Route::group(['middleware' => ['auth']], function() {
        Route::get('me', 'AuthController@me');
    });
});

Route::group(['middleware' => ['auth']], function() {
    Route::resource('categories', 'CategoryController', ['except' => ['create', 'edit', 'index', 'show']]);
    Route::resource('products.photos', 'ProductPhotoController', ['except' => ['create', 'edit']]);
    Route::resource('products', 'ProductController', ['except' => ['create', 'edit', 'show']]);
    Route::post('categories-update/{id}', 'CategoryController@update');
    Route::post('products-update/{id}', 'ProductController@update');
});

Route::get('products/byCategories/{category_id}', 'ProductController@byCategories');
Route::get('products/{category}/{product}', 'ProductController@show');
Route::get('categories', 'CategoryController@index');
Route::get('categories/{category}/products', 'CategoryController@products');
Route::get('product/{id}', 'ProductController@showProduct');
Route::get('photos/{id}', 'ProductPhotoController@index');
Route::post('email', 'EmailController@send');

// Cart
Route::get('cart/{hash?}', 'CartController@index');
Route::get('cart/{hash}/total', 'CartController@getTotal');
Route::post('cart', 'CartController@store');
Route::put('cart', 'CartController@update');
