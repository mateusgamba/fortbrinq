<?php

Route::get('/', 'PublicController@index');
Route::get('/sobre', 'PublicController@sobre');
Route::get('/produtos/{slugCategory?}', 'PublicController@produtos');
Route::get('/produtos/{slugCategory}/{slugProduct}', 'PublicController@produto');
Route::get('/contato', 'PublicController@contato');
Route::post('/contato/enviar', 'PublicController@contatoEnviar');
//Route::post('/orcamento/enviar/{slugCategory}/{slugProduct}', 'PublicController@orcamentoEnviar');
Route::get('/carrinho', 'PublicController@carrinho');
Route::post('/carrinho/enviar', 'PublicController@carrinhoEnviar');
Route::get('/carrinho/enviado', 'PublicController@carrinhoEnviadoSuccesso');
Route::get('/carrinho/excluir/{hash}/{id}', 'PublicController@carrinhoExcluirItem');

Route::view('/admin/{path?}', 'admin.app');
