<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
        'hash',
        'product_id',
        'amount'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public static function getOrder($hash)
    {
       return Cart::where('hash', $hash)
        ->select('id', 'product_id', 'amount')
        ->with(['product' => function($query) {
            $query->select('id', 'name', 'category_id', 'slug', 'photo')
                ->with(['category:id,name,slug']);
        }])
        ->orderBy('id', 'ASC')
        ->get();
    }
}
