<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Storage;

use App\ProductPhoto;

class ProductPhotoController extends Controller
{
    public function index(int $product)
    {
        $data = ProductPhoto::where('product_id', $product)
            ->get();
        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => \Lang::get('messages.itemsReturned')
        ], 200);
    }

    public function store(Request $request, int $product)
    {
        $data = $request->only('photo', 'description', 'order');

        $validator = Validator::make($data, [
            'photo' => 'required|mimes:jpeg,bmp,png,jpg',
            'description' => 'max:5120'
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }

        $image = $request->file('photo');
        $filename = $request->file('photo')->getClientOriginalName();
        $dash = strripos($filename, '.');
        $name = str_slug(substr($filename, 0, $dash), '-');
        $extension = substr($filename, $dash + 1);
        $fileNameToStore = $name . '.' . $extension;
        $request->file('photo')->storeAs('/', $fileNameToStore);

        $data['product_id'] = $product;
        $data['photo'] = $fileNameToStore;
        $photo = ProductPhoto::create($data);

        $response = [
            'success' => true,
            'data' => $photo,
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }

    public function show(int $product, int $productPhoto)
    {
        $photo = ProductPhoto::find($productPhoto);
        $response = [
            'success' => true,
            'data' => $photo,
            'message' => \Lang::get('messages.itemReturned')
        ];

        return response()->json($response, 200);
    }

    public function update(Request $request, int $idProduct, int $idPhoto)
    {
        $data = $request->only('description');

        $validator = Validator::make($data, [
            'description' => 'max:5120'
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }

        $photo = ProductPhoto::find($idPhoto);

        $photo->description = $data['description'];
        $photo->save();

        $response = [
            'success' => true,
            'data' => $photo,
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }

    public function destroy(int $idProduct, int $idPhoto)
    {
        $photo = ProductPhoto::find($idPhoto);
        $photo->delete();

        $response = [
            'success' => true,
            'data' => $photo,
            'message' => \Lang::get('messages.deleteSuccess')
        ];

        return response()->json($response, 200);
    }

    public function getPhoto($photo)
    {
        $exists = Storage::disk('public')->exists($photo);
        if (!$exists) {
            abort(404);
        }

        $file = Storage::disk('public')->get($photo);
        $mime = substr($photo, -3);
        return response($file, 200)->header('Content-Type', 'image/'.$mime);
    }
}