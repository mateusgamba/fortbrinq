<?php

namespace App\Http\Controllers;

use Redirect;
use Validator;
use View;

use App\Cart;
use App\Category;
use App\Product;
use App\Mail\ContactUs;
use App\Mail\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function index()
    {
        $categories = Category::getCategories();
        return View::make('index', compact('categories'));
    }

    public function sobre()
    {
        return View::make('sobre');
    }

    public function produtos($slugCategory = '')
    {
        $categories = Category::query();
        if (!empty($slugCategory) && $slugCategory !== 'todos') {
            $categories = Category::where('slug', $slugCategory);
        }
        $categories = $categories->select('id', 'name', 'slug')
            ->with(['products' => function($query) {
                $query
                    ->select('id', 'name', 'slug', 'link_3d', 'category_id', 'photo')
                    ->orderBy('order');
              }])
            ->get();
        $breadcrumb = empty($slugCategory) || $slugCategory === 'todos' ? 'Todos' : $categories[0]->name;
        return View::make('produtos', compact('categories', 'breadcrumb'));
    }

    public function produto($slugCategory, $slugProduct)
    {
        $product = Product::where('slug', $slugProduct)
            ->select('id', 'name', 'description', 'link_3d', 'category_id', 'technical_description', 'slug', 'photo')
            ->with('category:id,name,slug')
            ->with('photos:photo,product_id,description')
            ->first();

        $slugCategory = $slugCategory === 'todos' ? 'todos' : $product->category->slug;

        return View::make('produto', compact('product', 'slugCategory'));
    }

    public function contato()
    {
        return View::make('contato');
    }

    public function contatoEnviar(Request $request)
    {
        $inputs = $request->only(
            'nome',
            'email',
            'telefone',
            'cidade',
            'estado',
            'perfil',
            'mensagem'
        );

        $rules = [
            'nome' => 'required|string|min:3|max:255',
            'email' => 'required|string|email|max:255',
            'telefone' => 'required|string|min:8|max:50',
            'cidade' => 'required|min:3|max:100',
            'estado' => 'required|min:2|max:100',
            'perfil' => 'required|max:100',
            'mensagem' => 'required|min:3'
        ];

        $validator = Validator::make($inputs, $rules);

        if($validator->fails()) {
            return Redirect::to('contato')->withErrors($validator)->withInput();
        }

        $perfil = '';
        if ($inputs['perfil']==='sindico') {
            $perfil = 'Síndico';
        } elseif ($inputs['perfil']==='construtora') {
            $perfil = 'Construtora';
        } elseif ($inputs['perfil']==='escola-creche') {
            $perfil = 'Escola/Creche';
        } elseif ($inputs['perfil']==='cliente-final') {
            $perfil = 'Cliente final';
        }

        Mail::to(env('MAIL_FROM_ADDRESS'))
            ->send(
                new ContactUs(
                    $inputs['nome'],
                    $inputs['email'],
                    $inputs['telefone'],
                    $inputs['cidade'].' / '.$inputs['estado'],
                    $perfil,
                    'Contato via Website',
                    $inputs['mensagem'],
                    '',
                    ''
                )
            );

        return Redirect::to('contato')->with('success','Item created successfully!');
    }

    public function orcamentoEnviar($slugCategory, $slugProduct, Request $request)
    {
        $inputs = $request->only(
            'nome',
            'email',
            'telefone',
            'cidade',
            'estado',
            'perfil',
            'mensagem',
            'product_id'
        );

        $rules = [
            'nome' => 'required|string|min:3|max:255',
            'email' => 'required|string|email|max:255',
            'telefone' => 'required|string|min:8|max:50',
            'cidade' => 'required|min:3|max:100',
            'estado' => 'required|min:2|max:100',
            'perfil' => 'required|max:100',
            'mensagem' => 'required|min:3',
            'product_id' => 'required|exists:products,id',
        ];

        $validator = Validator::make($inputs, $rules);

        if($validator->fails()) {
            return Redirect::to("produtos/{$slugCategory}/{$slugProduct}#orcamento")->withErrors($validator)->withInput();
        }

        $product = Product::find($inputs['product_id']);
        $categoryName = $product->category->name;
        $productName = $product->name;

        $perfil = '';
        if ($inputs['perfil']==='sindico') {
            $perfil = 'Síndico';
        } elseif ($inputs['perfil']==='construtora') {
            $perfil = 'Construtora';
        } elseif ($inputs['perfil']==='escola-creche') {
            $perfil = 'Escola/Creche';
        } elseif ($inputs['perfil']==='cliente-final') {
            $perfil = 'Cliente final';
        }

        Mail::to(env('MAIL_FROM_ADDRESS'))
            ->send(
                new ContactUs(
                    $inputs['nome'],
                    $inputs['email'],
                    $inputs['telefone'],
                    $inputs['cidade'].' / '.$inputs['estado'],
                    $perfil,
                    'Orçamento via Website',
                    $inputs['mensagem'],
                    $categoryName,
                    $productName
                )
            );

        return Redirect::to("produtos/{$slugCategory}/{$slugProduct}#orcamento")->with('success','Item created successfully!');
    }

    public function carrinho()
    {
        return View::make('carrinho');
    }

    public function carrinhoEnviar(Request $request)
    {
        $inputs = $request->only(
            'nome',
            'email',
            'telefone',
            'cidade',
            'estado',
            'perfil',
            'mensagem',
            'hash_products'
        );

        $rules = [
            'nome' => 'required|string|min:3|max:255',
            'email' => 'required|string|email|max:255',
            'telefone' => 'required|string|min:8|max:50',
            'cidade' => 'required|min:3|max:100',
            'estado' => 'required|min:2|max:100',
            'perfil' => 'required|max:100',
            'hash_products' => 'required|exists:carts,hash'
        ];

        $validator = Validator::make($inputs, $rules);

        if($validator->fails()) {
            return Redirect::to('carrinho')
                ->with('enableOrcamento', 1)
                ->withErrors($validator)
                ->withInput();
        }

        $products = Cart::getOrder($inputs['hash_products']);

        $perfil = '';
        if ($inputs['perfil']==='sindico') {
            $perfil = 'Síndico';
        } elseif ($inputs['perfil']==='construtora') {
            $perfil = 'Construtora';
        } elseif ($inputs['perfil']==='escola-creche') {
            $perfil = 'Escola/Creche';
        } elseif ($inputs['perfil']==='cliente-final') {
            $perfil = 'Cliente final';
        }

        Mail::to(env('MAIL_FROM_ADDRESS'))
            ->send(
                new Order(
                    $inputs['nome'],
                    $inputs['email'],
                    $inputs['telefone'],
                    $inputs['cidade'].' / '.$inputs['estado'],
                    $perfil,
                    'Orçamento via Website',
                    $inputs['mensagem'],
                    $products
                )
            );
        return Redirect::to('carrinho/enviado');
    }

    public function carrinhoEnviadoSuccesso()
    {
        return View::make('carrinhoEnviado');
    }

    public function carrinhoExcluirItem($hash, $id)
    {
        $inputs = [
            'hash' => $hash,
            'id' =>$id
        ];

        $rules = [
            'hash' => 'required|exists:carts,hash',
            'id' => 'required|exists:carts,id'
        ];

        $validator = Validator::make($inputs, $rules);

        if($validator->fails()) {
            return Redirect::to('carrinho')->withErrors($validator)->withInput();
        }

        Cart::where(['id' => $id, 'hash' => $hash])->delete();

        return Redirect::to('carrinho');
    }
}
