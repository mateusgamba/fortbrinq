<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use Validator;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::all();
        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => \Lang::get('messages.itemsReturned')
        ], 200);
    }

    public function store(Request $request)
    {
        $data = $request->only(
            'name',
            'description',
            'link_3d',
            'category_id',
            'order',
            'slug',
            'technical_description',
            'photo'
        );

        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'description' => 'max:5120',
            'link_3d' => 'max:2048',
            'category_id' => 'required|integer|exists:categories,id',
            'order' => 'required|integer',
            'photo' => 'nullable|mimes:jpeg,bmp,png,jpg',
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }

        if ($request->file('photo')) {
            $image = $request->file('photo');
            $filename = $request->file('photo')->getClientOriginalName();
            $dash = strripos($filename, '.');
            $name = str_slug(substr($filename, 0, $dash), '-');
            $extension = substr($filename, $dash + 1);
            $fileNameToStore = $name . '.' . $extension;
            $request->file('photo')->storeAs('/', $fileNameToStore);
            $data['photo'] = $fileNameToStore;
        }

        $data['slug'] = $data['name'];

        $product = Product::create($data);

        $response = [
            'success' => true,
            'data' => $product,
            'slug' => $product->category->slug,
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }

    public function show(Product $product)
    {
        $data = Product::where('slug', $product)
            ->whereHas('category', function ($query) use ($category) {
                $query->where('slug', $category);
            })
            ->with('category')
            ->with('photos')
            ->first();

        $response = [
            'success' => true,
            'data' => $data,
            'message' => \Lang::get('messages.itemReturned')
        ];

        return response()->json($response, 200);
    }

    public function update(string $id, Request $request)
    {
        $data = $request->only(
            'name',
            'description',
            'link_3d',
            'category_id',
            'order',
            'slug',
            'technical_description',
            'photo'
        );

        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'description' => 'max:5120',
            'link_3d' => 'max:2048',
            'category_id' => 'required|integer|exists:categories,id',
            'order' => 'required|integer',
            'photo' => 'nullable|mimes:jpeg,bmp,png,jpg',
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }


        $product = Product::find($id);

        $product->name = $data['name'];
        $product->description = $data['description'];
        $product->order = $data['order'];
        $product->link_3d = $data['link_3d'];
        $product->technical_description = $data['technical_description'];

        if ($request->file('photo')) {
            $image = $request->file('photo');
            $filename = $request->file('photo')->getClientOriginalName();
            $dash = strripos($filename, '.');
            $name = str_slug(substr($filename, 0, $dash), '-');
            $extension = substr($filename, $dash + 1);
            $fileNameToStore = $name . '.' . $extension;
            $request->file('photo')->storeAs('/', $fileNameToStore);
            $product->photo = $fileNameToStore;
        }

        $product->save();

        $response = [
            'success' => true,
            'data' => $product,
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }

    public function destroy(Product $product)
    {
        $product->delete();

        $response = [
            'success' => true,
            'data' => $product,
            'message' => \Lang::get('messages.deleteSuccess')
        ];

        return response()->json($response, 200);
    }

    public function byCategories(string $category_id)
    {
        $products = Product::where('category_id', $category_id)
            ->orderBy('order')
            ->get();

        return response()->json([
            'success' => true,
            'data' => $products,
            'message' => \Lang::get('messages.itemsReturned')
        ], 200);

    }

    public function showProduct(string $id)
    {
        $product = Product::find($id);

        return response()->json([
            'success' => true,
            'data' => $product,
            'message' => \Lang::get('messages.itemsReturned')
        ], 200);
    }
}
