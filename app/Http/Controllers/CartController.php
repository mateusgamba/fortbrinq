<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;

use Validator;

class CartController extends Controller
{
    public function index($hash = '')
    {
        $products = [];
        if (!empty($hash)) {
            $products = Cart::getOrder($hash);
        }

        return response()->json([
            'success' => true,
            'data' => $products,
            'message' => \Lang::get('messages.itemsReturned')
        ], 200);
    }

    public function store(Request $request)
    {
        $data = $request->only('hash', 'product_id');

        $validator = Validator::make($data, [
            'hash' => 'required|max:10',
            'product_id' => 'required|exists:products,id'
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }

        $order = Cart::where(['hash' => $data['hash'], 'product_id' => $data['product_id']])
            ->select('id', 'amount')
            ->first();

        if (empty($order)) {
            $data['amount'] = 1;
            $cart = Cart::create($data);
        } else {
            $cart = Cart::find($order->id);
            $cart->amount = $order->amount + 1;
            $cart->save();
        }

        $response = [
            'success' => true,
            'data' => $cart,
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }

    public function update(Request $request)
    {
        $data = $request->only('hash', 'id', 'value');

        $validator = Validator::make($data, [
            'hash' => 'required|exists:carts,hash',
            'id' => 'required|exists:carts,id'
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }

        $order = Cart::where(['hash' => $data['hash'], 'id' => $data['id']])
            ->select('amount')
            ->first();

        $cart = Cart::find($data['id']);
        $cart->amount = $data['value'] ? $order->amount + 1 : $order->amount - 1;
        $cart->save();

        $response = [
            'success' => true,
            'data' => $cart,
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }

    public function getTotal($hash)
    {
        $inputs = [ 'hash' => $hash ];

        $rules = [ 'hash' => 'required|exists:carts,hash' ];

        $validator = Validator::make($inputs, $rules);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }

        $total = Cart::where('hash', $hash)->sum('amount');

        $response = [
            'success' => true,
            'data' => ['total' => $total],
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }
}
