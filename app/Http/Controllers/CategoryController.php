<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use Validator;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::getCategories();
        return response()->json([
            'success' => true,
            'data' => $categories,
            'message' => \Lang::get('messages.itemsReturned')
        ], 200);
    }

    public function store(Request $request)
    {
        $data = $request->only('name', 'description', 'order', 'image');

        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'description' => 'max:5120',
            'order' => 'required|integer',
            'photo' => 'nullable|mimes:jpeg,bmp,png,jpg',
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }

        if ($request->file('image')) {
            $image = $request->file('image');
            $filename = $request->file('image')->getClientOriginalName();
            $dash = strripos($filename, '.');
            $name = str_slug(substr($filename, 0, $dash), '-');
            $extension = substr($filename, $dash + 1);
            $fileNameToStore = $name . '.' . $extension;
            $request->file('image')->storeAs('/', $fileNameToStore);
            $data['image'] = $fileNameToStore;
        }

        $data['slug'] = $data['name'];

        $category = Category::create($data);

        $response = [
            'success' => true,
            'data' => $category,
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }

    public function show(Category $category)
    {
        $response = [
            'success' => true,
            'data' => $category,
            'message' => \Lang::get('messages.itemReturned')
        ];

        return response()->json($response, 200);
    }

    public function showBySlug($slugCategory)
    {
        $category = Category::select('id', 'name', 'slug')
            ->where('slug', $slugCategory)
            ->with(['products' => function($query) {
                $query->select('id', 'name', 'slug', 'link_3d', 'category_id')
                    ->orderBy('order')
                    ->with(['photos:product_id,product_id']);
              }])
            ->first();
        return response()->json([
            'success' => true,
            'data' => $category,
            'message' => \Lang::get('messages.itemReturned')
        ], 200);
    }

    public function update(int $id, Request $request)
    {
        $data = $request->only('name', 'description', 'order', 'image');

        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'description' => 'max:5120',
            'order' => 'required|integer',
            'photo' => 'nullable|mimes:jpeg,bmp,png,jpg',
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->messages(),
                'message' => \Lang::get('messages.validationFields')
            ], 400);
        }

        $category = Category::find($id);

        $category->name = $data['name'];
        $category->description = $data['description'];
        $category->order = $data['order'];


        if ($request->file('image')) {
            $image = $request->file('image');
            $filename = $request->file('image')->getClientOriginalName();
            $dash = strripos($filename, '.');
            $name = str_slug(substr($filename, 0, $dash), '-');
            $extension = substr($filename, $dash + 1);
            $fileNameToStore = $name . '.' . $extension;
            $request->file('image')->storeAs('/', $fileNameToStore);
            $category->image = $fileNameToStore;
        }

        $category->save();

        $response = [
            'success' => true,
            'data' => $category,
            'message' => \Lang::get('messages.saveSuccess')
        ];

        return response()->json($response, 200);
    }

    public function destroy(Category $category)
    {
        $category->delete();

        $response = [
            'success' => true,
            'data' => $category,
            'message' => \Lang::get('messages.deleteSuccess')
        ];

        return response()->json($response, 200);
    }

    public function products(string $category)
    {
        $data = Category::where("slug", $category)->with('products.photos')->first();

        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => \Lang::get('messages.itemsReturned')
        ], 200);
    }
}
