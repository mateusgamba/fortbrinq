<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class Order extends Mailable
{
    public $nome, $email, $telefone, $cidade, $perfil, $assunto, $mensagem, $products;

    public function __construct(
        $nome,
        $email,
        $telefone,
        $cidade,
        $perfil,
        $assunto,
        $mensagem,
        $products
    )
    {
        $this->nome = $nome;
        $this->email = $email;
        $this->telefone = $telefone;
        $this->cidade = $cidade;
        $this->perfil = $perfil;
        $this->assunto = $assunto;
        $this->mensagem = $mensagem;
        $this->products = $products;
    }

    public function build()
    {
        return $this->view('templateOrder')
            ->from(env('MAIL_FROM_ADDRESS'))
            ->to(env('MAIL_DEFAULT'))
            ->subject($this->assunto)
            ->replyTo($this->email, $this->nome);
    }
}
