<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class ContactUs extends Mailable
{
    public $nome, $email, $telefone, $cidade, $perfil, $assunto, $mensagem, $categoryName, $productName;

    public function __construct(
        $nome,
        $email,
        $telefone,
        $cidade,
        $perfil,
        $assunto,
        $mensagem,
        $categoryName = '',
        $productName = ''
    )
    {
        $this->nome = $nome;
        $this->email = $email;
        $this->telefone = $telefone;
        $this->cidade = $cidade;
        $this->perfil = $perfil;
        $this->assunto = $assunto;
        $this->mensagem = $mensagem;
        $this->categoryName = $categoryName;
        $this->productName = $productName;
    }

    public function build()
    {
        return $this->view('templateEmail')
            ->from(env('MAIL_FROM_ADDRESS'))
            ->to(env('MAIL_DEFAULT'))
            ->subject($this->assunto)
            ->replyTo($this->email, $this->nome);
    }
}