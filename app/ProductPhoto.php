<?php

namespace App;

use Storage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPhoto extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'photo',
        'description',
        'product_id',
        'order',
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    protected $appends = ['photo_path'];

    protected function getPhotoPathAttribute()
    {
        return Storage::url($this->photo);
    }
}
