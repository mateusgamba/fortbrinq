<?php

namespace App;

use Storage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'technical_description',
        'link_3d',
        'category_id',
        'order',
        'slug',
        'photo'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value, '-');
    }

    public function photos()
    {
        return $this->hasMany('App\ProductPhoto');
    }

    protected $appends = ['photo_path'];

    protected function getPhotoPathAttribute()
    {
        return Storage::url($this->photo);
    }
}
