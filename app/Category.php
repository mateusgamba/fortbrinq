<?php

namespace App;

use Storage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'description', 'image', 'order', 'slug'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value, '-');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public static function getCategories()
    {
        return Category::select('name', 'image', 'slug', 'order')
            ->orderBy('order')
            ->get();
    }

    protected $appends = ['image_path'];

    protected function getImagePathAttribute()
    {
        return Storage::url($this->image);
    }
}
