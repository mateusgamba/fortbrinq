<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        $items = [
            // Playground de Madeira em Eucalipto Tratado
            [
                'id' => 1,
                'name' => 'D-01',
                'category_id' => 1,
                'order' => 10,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.'
            ],
            [
                'id' => 2,
                'name' => 'D-02',
                'category_id' => 1,
                'order' => 20,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 3,
                'name' => 'D-03',
                'category_id' => 1,
                'order' => 30,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 4,
                'name' => 'D-04',
                'category_id' => 1,
                'order' => 40,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 5,
                'name' => 'D-07',
                'category_id' => 1,
                'order' => 50,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 6,
                'name' => 'D-09',
                'category_id' => 1,
                'order' => 60,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 7,
                'name' => 'D-10',
                'category_id' => 1,
                'order' => 70,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 8,
                'name' => 'D-11',
                'category_id' => 1,
                'order' => 80,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 9,
                'name' => 'D-13',
                'category_id' => 1,
                'order' => 90,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 10,
                'name' => 'D-14',
                'category_id' => 1,
                'order' => 100,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó',
            ],
            [
                'id' => 11,
                'name' => 'D-15',
                'category_id' => 1,
                'order' => 110,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 12,
                'name' => 'D-16',
                'category_id' => 1,
                'order' => 120,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó',
            ],
            [
                'id' => 13,
                'name' => 'D-17',
                'category_id' => 1,
                'order' => 130,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            [
                'id' => 14,
                'name' => 'D-18',
                'category_id' => 1,
                'order' => 140,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó',
            ],
            [
                'id' => 15,
                'name' => 'D-19',
                'category_id' => 1,
                'order' => 150,
                'description' => 'Parque infantil fabricado em eucalipto roliço tratado, e ferragens galvanizadas com pintura eletrostática a pó.',
            ],
            // Brinquedos Individuais
            [
                'id' => 16,
                'name' => 'F-011',
                'category_id' => 2,
                'order' => 10,
            ],
            [
                'id' => 17,
                'name' => 'F-012',
                'category_id' => 2,
                'order' => 20,
            ],
            [
                'id' => 18,
                'name' => 'F-013',
                'category_id' => 2,
                'order' => 30,
                'description' => ''
            ],
            [
                'id' => 19,
                'name' => 'F-02',
                'category_id' => 2,
                'order' => 40,
                'description' => ''
            ],
            [
                'id' => 20,
                'name' => 'F-021',
                'category_id' => 2,
                'order' => 50,
                'description' => ''
            ],
            [
                'id' => 21,
                'name' => 'F-023',
                'category_id' => 2,
                'order' => 60,
                'description' => ''
            ],
            [
                'id' => 22,
                'name' => 'F-024',
                'category_id' => 2,
                'order' => 70,
                'description' => ''
            ],
            [
                'id' => 23,
                'name' => 'F-029',
                'category_id' => 2,
                'order' => 80,
                'description' => ''
            ],
            [
                'id' => 24,
                'name' => 'F-030',
                'category_id' => 2,
                'order' => 90,
                'description' => ''
            ],
            [
                'id' => 25,
                'name' => 'F-032',
                'category_id' => 2,
                'order' => 100,
                'description' => ''
            ],
            [
                'id' => 26,
                'name' => 'F-035',
                'category_id' => 2,
                'order' => 110,
                'description' => ''
            ],
            [
                'id' => 27,
                'name' => 'F-039',
                'category_id' => 2,
                'order' => 120,
                'description' => ''
            ],
            [
                'id' => 28,
                'name' => 'F-046',
                'category_id' => 2,
                'order' => 130,
                'description' => ''
            ],
            [
                'id' => 29,
                'name' => 'F-047',
                'category_id' => 2,
                'order' => 140,
                'description' => ''
            ],
            [
                'id' => 30,
                'name' => 'F-049',
                'category_id' => 2,
                'order' => 150,
                'description' => ''
            ],
            [
                'id' => 31,
                'name' => 'F-052',
                'category_id' => 2,
                'order' => 160,
                'description' => ''
            ],
            [
                'id' => 32,
                'name' => 'F-07',
                'category_id' => 2,
                'order' => 170,
                'description' => ''
            ],
            [
                'id' => 33,
                'name' => 'F-08',
                'category_id' => 2,
                'order' => 180,
                'description' => ''
            ],
            [
                'id' => 34,
                'name' => 'F-101',
                'category_id' => 2,
                'order' => 190,
                'description' => ''
            ],
            [
                'id' => 35,
                'name' => 'F-102',
                'category_id' => 2,
                'order' => 200,
                'description' => ''
            ],
            [
                'id' => 36,
                'name' => 'F-103',
                'category_id' => 2,
                'order' => 210,
                'description' => ''
            ],
            [
                'id' => 37,
                'name' => 'F-104',
                'category_id' => 2,
                'order' => 220,
                'description' => ''
            ],
            [
                'id' => 38,
                'name' => 'F-106',
                'category_id' => 2,
                'order' => 230,
                'description' => ''
            ],
            [
                'id' => 39,
                'name' => 'F-107',
                'category_id' => 2,
                'order' => 240,
                'description' => ''
            ],
            [
                'id' => 40,
                'name' => 'F-108',
                'category_id' => 2,
                'order' => 260,
                'description' => ''
            ],
            [
                'id' => 41,
                'name' => 'F-109',
                'category_id' => 2,
                'order' => 270,
                'description' => ''
            ],
            // Grama Sintética
            [
                'id' => 42,
                'name' => 'Grama Sintética',
                'category_id' => 3,
                'order' => 10,
                'description' => ''
            ],
            // Mobiliário Urbano
            [
                'id' => 43,
                'name' => 'M-02',
                'category_id' => 4,
                'order' => 10
            ],
            [
                'id' => 44,
                'name' => 'M-05',
                'category_id' => 4,
                'order' => 20
            ],
            [
                'id' => 45,
                'name' => 'MB-01',
                'category_id' => 4,
                'order' => 30
            ],
            [
                'id' => 46,
                'name' => 'MB-02',
                'category_id' => 4,
                'order' => 40
            ],
            [
                'id' => 47,
                'name' => 'MB-03',
                'category_id' => 4,
                'order' => 50
            ],
            [
                'id' => 48,
                'name' => 'MB-04',
                'category_id' => 4,
                'order' => 60
            ],
            [
                'id' => 49,
                'name' => 'MB-05',
                'category_id' => 4,
                'order' => 70
            ],
            [
                'id' => 50,
                'name' => 'MB-06',
                'category_id' => 4,
                'order' => 80
            ],
            // Playground de Madeira Plástica
            [
                'id' => 51,
                'name' => 'FB-01',
                'category_id' => 5,
                'order' => 10,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 52,
                'name' => 'FB-02',
                'category_id' => 5,
                'order' => 20,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 53,
                'name' => 'FB-03',
                'category_id' => 5,
                'order' => 30,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 54,
                'name' => 'FB-04',
                'category_id' => 5,
                'order' => 40,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 55,
                'name' => 'FB-05',
                'category_id' => 5,
                'order' => 50,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 56,
                'name' => 'FB-06',
                'category_id' => 5,
                'order' => 60,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 57,
                'name' => 'FB-07',
                'category_id' => 5,
                'order' => 70,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 58,
                'name' => 'FB-09',
                'category_id' => 5,
                'order' => 80,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 59,
                'name' => 'FB-10',
                'category_id' => 5,
                'order' => 90,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 60,
                'name' => 'FB-11',
                'category_id' => 5,
                'order' => 100,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 61,
                'name' => 'FB-12',
                'category_id' => 5,
                'order' => 120,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 62,
                'name' => 'FB-13',
                'category_id' => 5,
                'order' => 130,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 63,
                'name' => 'FB-14',
                'category_id' => 5,
                'order' => 140,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 64,
                'name' => 'FB-15',
                'category_id' => 5,
                'order' => 150,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 65,
                'name' => 'FB-16',
                'category_id' => 5,
                'order' => 160,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática.'
            ],
            [
                'id' => 66,
                'name' => 'FB-17',
                'category_id' => 5,
                'order' => 170,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 67,
                'name' => 'FB-18',
                'category_id' => 5,
                'order' => 180,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 68,
                'name' => 'FB-20',
                'category_id' => 5,
                'order' => 190,
                'description' => 'Parque infantil colorido em madeira plástica, com ferragens galvanizadas e pintura eletrostática a pó contendo.'
            ],
            [
                'id' => 69,
                'name' => 'FB-21',
                'category_id' => 5,
                'order' => 200,
                'description' => ''
            ]
        ];

        foreach ($items as $item) {
            $item['slug'] = str_slug($item['name'], '-');
            factory(\App\Product::class, 1)->create($item);
        }
    }
}
