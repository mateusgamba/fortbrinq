<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'name' => 'Playground de Madeira em Eucalipto Tratado',
                'image' => 'playground-de-madeira-em-eucalipto-tratado.png',
                'order' => 10
            ],
            [
                'id' => 2,
                'name' => 'Brinquedos Individuais',
                'image' => 'brinquedos-individuais.png',
                'order' => 20
            ],
            [
                'id' => 3,
                'name' => 'Grama Sintética',
                'image' => 'grama-sintetica.jpg',
                'order' => 30
            ],
            [
                'id' => 4,
                'name' => 'Mobiliário Urbano',
                'image' => 'mobiliario-urbano.jpg',
                'order' => 40
            ],
            [
                'id' => 5,
                'name' => 'Playground de Madeira Plástica',
                'image' => 'playground-de-madeira-plastica.jpg',
                'order' => 50
            ]
        ];
        foreach ($items as $item) {
            $item['slug'] = str_slug($item['name'], '-');
            factory(\App\Category::class, 1)->create($item);
        }
    }
}
