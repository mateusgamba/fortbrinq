<?php

use Illuminate\Database\Seeder;

class ProductPhotosTableSeeder extends Seeder
{
    public function run()
    {
        $items = [
            // Playground de Madeira em Eucalipto Tratado
            [
                'product_id' => 1,
                'photo' => 'd-01.png',
                'order' => 10
            ],
            [
                'product_id' => 2,
                'photo' => 'd-02.png',
                'order' => 20,
            ],
            [
                'product_id' => 3,
                'photo' => 'd-03.png',
                'order' => 30,
            ],
            [
                'product_id' => 4,
                'photo' => 'd-04.png',
                'order' => 30,
            ],
            [
                'product_id' => 5,
                'photo' => 'd-07.png',
                'order' => 30,
            ],
            [
                'product_id' => 6,
                'photo' => 'd-09.png',
                'order' => 30,
            ],
            [
                'product_id' => 7,
                'photo' => 'd-10.png',
                'order' => 30,
            ],
            [
                'product_id' => 8,
                'photo' => 'd-11.png',
                'order' => 30,
            ],
            [
                'product_id' => 9,
                'photo' => 'd-13.png',
                'order' => 30,
            ],
            [
                'product_id' => 10,
                'photo' => 'd-14.png',
                'order' => 30,
            ],
            [
                'product_id' => 11,
                'photo' => 'd-15.png',
                'order' => 30,
            ],
            [
                'product_id' => 12,
                'photo' => 'd-16.png',
                'order' => 30,
            ],
            [
                'product_id' => 13,
                'photo' => 'd-17.png',
                'order' => 30,
            ],
            [
                'product_id' => 14,
                'photo' => 'd-18.png',
                'order' => 30,
            ],
            [
                'product_id' => 15,
                'photo' => 'd-19.png',
                'order' => 30,
            ],
            // Brinquedos Individuais
            [
                'product_id' => 16,
                'photo' => 'f-011.jpg',
                'order' => 10,
            ],
            [
                'product_id' => 17,
                'photo' => 'f-012.jpg',
                'order' => 20,
            ],
            [
                'product_id' => 18,
                'photo' => 'f-013.jpg',
                'order' => 30,
            ],
            [
                'product_id' => 19,
                'photo' => 'f-02.jpg',
                'order' => 40,
            ],
            [
                'product_id' => 20,
                'photo' => 'f-021.jpg',
                'order' => 50,
            ],
            [
                'product_id' => 21,
                'photo' => 'f-023.png',
                'order' => 60,
            ],
            [
                'product_id' => 22,
                'photo' => 'f-024.jpg',
                'order' => 70,
            ],
            [
                'product_id' => 23,
                'photo' => 'f-029.jpg',
                'order' => 80,
            ],
            [
                'product_id' => 24,
                'photo' => 'f-030.jpg',
                'order' => 90,
            ],
            [
                'product_id' => 25,
                'photo' => 'f-032.jpg',
                'order' => 100,
            ],
            [
                'product_id' => 26,
                'photo' => 'f-035.jpg',
                'order' => 110,
            ],
            [
                'product_id' => 27,
                'photo' => 'f-039.jpg',
                'order' => 120,
            ],
            [
                'product_id' => 28,
                'photo' => 'f-046.jpg',
                'order' => 130,
            ],
            [
                'product_id' => 29,
                'photo' => 'f-047.jpg',
                'order' => 140,
            ],
            [
                'product_id' => 30,
                'photo' => 'f-049.jpg',
                'order' => 150,
            ],
            [
                'product_id' => 31,
                'photo' => 'f-052.jpg',
                'order' => 160,
            ],
            [
                'product_id' => 32,
                'photo' => 'f-07.png',
                'order' => 170,
            ],
            [
                'product_id' => 33,
                'photo' => 'f-08.jpg',
                'order' => 180,
            ],
            [
                'product_id' => 34,
                'photo' => 'f-101.png',
                'order' => 190,
            ],
            [
                'product_id' => 35,
                'photo' => 'f-102.png',
                'order' => 200,
            ],
            [
                'product_id' => 36,
                'photo' => 'f-103.png',
                'order' => 210,
            ],
            [
                'product_id' => 37,
                'photo' => 'f-104.png',
                'order' => 220,
            ],
            [
                'product_id' => 38,
                'photo' => 'f-106.png',
                'order' => 230,
            ],
            [
                'product_id' => 39,
                'photo' => 'f-107.png',
                'order' => 240,
            ],
            [
                'product_id' => 40,
                'photo' => 'f-108.jpg',
                'order' => 260,
            ],
            [
                'product_id' => 41,
                'photo' => 'f-109.jpg',
                'order' => 270,
            ],
            // Grama Sintética
            [
                'product_id' => 42,
                'photo' => 'grama-sintetica-product.jpg',
                'order' => 10,
            ],
            // Mobiliário Urbano
            [
                'product_id' => 43,
                'photo' => 'm-02.jpg',
                'order' => 10
            ],
            [
                'product_id' => 44,
                'photo' => 'm-05.jpg',
                'order' => 20
            ],
            [
                'product_id' => 45,
                'photo' => 'mb-01.png',
                'order' => 30
            ],
            [
                'product_id' => 46,
                'photo' => 'mb-02.png',
                'order' => 40
            ],
            [
                'product_id' => 49,
                'photo' => 'mb-05.png',
                'order' => 70
            ],
            [
                'product_id' => 50,
                'photo' => 'mb-06.jpg',
                'order' => 80
            ],
            // Playground de Madeira Plástica
            [
                'product_id' => 51,
                'photo' => 'fb-01.jpg',
                'order' => 10,
            ],
            [
                'product_id' => 52,
                'photo' => 'fb-02.jpg',
                'order' => 20,
            ],
            [
                'product_id' => 53,
                'photo' => 'fb-03.jpg',
                'order' => 30,
            ],
            [
                'product_id' => 54,
                'photo' => 'fb-04.jpg',
                'order' => 40,
            ],
            [
                'product_id' => 55,
                'photo' => 'fb-05.jpg',
                'order' => 50,
            ],
            [
                'product_id' => 56,
                'photo' => 'fb-06.jpg',
                'order' => 60,
            ],
            [
                'product_id' => 57,
                'photo' => 'fb-07.jpg',
                'order' => 70,
            ],
            [
                'product_id' => 58,
                'photo' => 'fb-09.jpeg',
                'order' => 80,
            ],
            [
                'product_id' => 59,
                'photo' => 'fb-10.jpeg',
                'order' => 90,
            ],
            [
                'product_id' => 60,
                'photo' => 'fb-11.jpg',
                'order' => 100,
            ],
            [
                'product_id' => 61,
                'photo' => 'fb-12.jpg',
                'order' => 120,
            ],
            [
                'product_id' => 62,
                'photo' => 'fb-13.jpeg',
                'order' => 130,
            ],
            [
                'product_id' => 63,
                'photo' => 'fb-14.jpeg',
                'order' => 140,
            ],
            [
                'product_id' => 64,
                'photo' => 'fb-15.jpeg',
                'order' => 150,
            ],
            [
                'product_id' => 65,
                'photo' => 'fb-16.png',
                'order' => 160,
            ],
            [
                'product_id' => 66,
                'photo' => 'fb-17.jpg',
                'order' => 170,
            ],
            [
                'product_id' => 67,
                'photo' => 'fb-18.jpg',
                'order' => 180,
            ],
            [
                'product_id' => 68,
                'photo' => 'fb-20.jpeg',
                'order' => 190,
            ],
            [
                'product_id' => 69,
                'photo' => 'fb-21.jpeg',
                'order' => 200,
            ]
        ];
        foreach ($items as $item) {
            factory(\App\ProductPhoto::class, 1)->create($item);
        }
    }
}
