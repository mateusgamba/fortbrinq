<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\User::class, 1)->create([
            'name' => 'Administrador',
            'email' => 'mateusgamba@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123123'),
            'remember_token' => Str::random(10),
            'activated' => 1
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Renato',
            'email' => 'contato@fortbrinq.com.br',
            'email_verified_at' => now(),
            'password' => bcrypt('renato2017x'),
            'remember_token' => Str::random(10),
            'activated' => 1
        ]);
    }
}
