<?php

return [
  'itemsReturned' => 'Items returned successfully.',
  'itemReturned' => 'Item returned successfully.',
  'validationFields' => 'Please check the indicated fields below and fix any errors before continuing.',
  'saveSuccess' => 'Successfully saved.',
  'deleteSuccess' => 'Successfully deleted.',
];