<?php

return [
  'itemsReturned' => 'Items returned successfully',
  'itemReturned' => 'Item returned successfully'
];