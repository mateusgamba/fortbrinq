<div style="font-family:tahoma">
  @if (!empty($productName))
  <p><strong>Produto: </strong>{{ $productName }} - {{ $categoryName }}</p>
  @endif
  <p><strong>Nome: </strong>{{ $nome }}</p>
  <p><strong>E-mail: </strong>{{ $email }}</p>
  <p><strong>Telefone: </strong>{{ $telefone }}</p>
  <p><strong>Cidade: </strong>{{ $cidade }}</p>
  <p><strong>Perfil: </strong>{{ $perfil }}</p>
  <p><strong>Mensagem: </strong>{!! nl2br($mensagem) !!}</p>
  <p><strong>Data de envio: </strong>{{ date('d-m-Y H:i:s') }}</p>
</div>