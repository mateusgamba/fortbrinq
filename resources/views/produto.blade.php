@extends('master')

@section('main')
@php
    if (empty($product->photo)) {
        $photo = '/img/produto-sem-imagem.gif';
    } else {
        $photo = $product->photo_path;
    }

    $descriptionCategory = $slugCategory === 'todos' ? 'Todos' : $product->category->name;
@endphp

<div class="container-fluid title-int">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2>Produto</h2>
                <span class="breadcrumbs">
                    <a href="{{{ url('/') }}}">Home</a>
                    /
                    <a href="{{{ url('/#produtos') }}}">Produtos</a>
                    /
                    <a href="{{{ url('/produtos') }}}/{{{ $slugCategory }}}">{{{ $descriptionCategory }}}</a>
                    /
                    {{{ $product->name }}}
                </span>
            </div>
            <div class="col">
                <div class="phone float-right">
                    <i class="fa fa-phone "></i>
                    <span>
                        Fale conosco<br />
                        <strong>(48) 3646-6651 </strong>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .tres html{
        overflow-x: hidden;
        overflow-y: hidden;
    }

    .img-thumbnail {
        width: 102px !important;
        height: 79px !important;
    }

    .btn-a:hover {
        opacity: 0.8 !important;
        color: #ffffff !important;
        background-color: #f57514 !important;
    }
</style>
<div class="container-fluid box-produto">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 tres px-0 order-1" style="overflow: hidden;">

                <div id="div-3d" class="{{{empty($product->link_3d) ? 'd-none' : '' }}}">
                    <iframe id="iframe3d" src="{{{ url('/') }}}/{{{ $product->link_3d }}}" style="border:none;height:430px;width:570px;overflow-y:hidden;overflow-x:hidden;"></iframe>
                </div>

                <figure id="div-photo" class="mb-0 pb-0 {{{!empty($product->link_3d) ? 'd-none' : '' }}}">
                    <img src="{{{ $photo }}}" id="img-primary" class="figure-img img-fluid rounded w-100 ml-2 mb-0" alt="{{{ $product->name }}}">
                </figure>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 order-3 order-md-2">
                <h2 class="title-produto-individual">{{{ $product->category->name }}}</h2>
                <span class="codigo-produto-individual">Código: {{{ $product->name }}}</span>
                <div class="mt-3 description-produto-individual">{!! nl2br($product->description) !!}</div>
                <div class="mt-5">
                <a href="javascript:void(0)" class="{{{empty($product->link_3d) ? 'd-none' : '' }}} mr-3"  onClick="girar()" id="btn-girar">
                    <img src="{{{ asset('/img/img_360.png')}}}" alt="Girar">
                </a>
                <a href="javascript:void(0)" class="btn-produto btn-a" id="btnAddProduct" data-id="{{{ $product->id }}}">Adicionar ao Carrinho</a>


                </div>
            </div>
            @if (count($product->photos)>0 || !empty($product->link_3d))
                <div class="col-xs-12 col-sm-12 col-md-6 tres pt-2 pl-1 pr-0 order-2 order-md-3">
                    <a href="{{{ $photo }}}" data-fancybox="gallery">
                        <img src="{{{ $photo }}}" class="img-thumbnail border-0">
                    </a>
                    @foreach ($product->photos as $itemPhoto)
                        <a href="{{{ $itemPhoto->photo_path }}}" data-fancybox="gallery" data-caption="{{{ $itemPhoto->description }}}">
                            <img src="{{{ $itemPhoto->photo_path }}}" class="img-thumbnail border-0">
                        </a>
                    @endforeach
                </div>
            @endif
        </div>

        @if (!empty($product->technical_description))
            <div class="row">
                <div class="col-12">
                    <a name="descricao"></a>
                    <h2 class="title-produto-individual-subtitles">Descri&ccedil;&atilde;o T&eacute;cnica</h2>
                    <div class="description-produto-individual">{!! nl2br($product->technical_description) !!}</div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection

<script>
    function girar() {
        document.getElementById('iframe3d').contentWindow.minhaFuncao();
    }

    function setImage(value, link3d) {
        if (link3d=='0') {
            var image = $(value).attr('src')
            $('#img-primary').attr('src', image);
            $("#div-3d, #btn-girar").addClass('d-none');
            $("#div-photo").removeClass('d-none');


        } else {
            $("#div-photo").addClass('d-none');
            $("#div-3d, #btn-girar").removeClass('d-none');
        }
    }
</script>
