<div style="font-family:tahoma">
  <p><strong>Nome: </strong>{{ $nome }}</p>
  <p><strong>E-mail: </strong>{{ $email }}</p>
  <p><strong>Telefone: </strong>{{ $telefone }}</p>
  <p><strong>Cidade: </strong>{{ $cidade }}</p>
  <p><strong>Perfil: </strong>{{ $perfil }}</p>
  <p><strong>Mensagem: </strong>{!! nl2br($mensagem) !!}</p>
  <p><strong>Data de envio: </strong>{{ date('d-m-Y H:i:s') }}</p>

  <table style="border:1px solid black">
    <thead>
      <tr>
        <th style="width:80%; text-align:left;">Produtos</th>
        <th style="width:10%; text-align:center;">Quantidade</th>
      </tr>
    </thead>
    <tbody>
    @php
      $count = 0;
    @endphp
    @foreach ($products as $product)
      @if ($product->amount>0)
        @php
          $count += $product->amount ;
        @endphp
        <tr>
          <td>{{{ $product->product->name }}}</td>
          <td style="text-align:center">{{{ $product->amount }}}</td>
        </tr>
      @endif
    @endforeach
    </tbody>
    <tfoot>
      <tr>
        <th style="text-align:left;">Quantidade total:</th>
        <th style="text-align:center">
        @php
          echo $count;
        @endphp
        </th>
      </tr>
    </tfoot>
  </table>

</div>