@extends('master')

@section('main')
<div class="container-fluid title-int">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>Sobre Nós</h2>
			</div>
			<div class="col">
				<div class="phone float-right">
					<i class="fa fa-phone "></i>
					<span>
						Fale conosco<br />
						<strong>(48) 3646-6651 </strong>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<a name="historia"></a>
<div class="container box-empresa">
	<div class="row">
		<div class="col-6">
			<p>
			Presente no mercado desde 1980 a Fortbrinq Parques Infantis é uma empresa hereditária que passou de uma geração de pai para filhos, temos como compromisso atender nossos  clientes com qualidade e seriedade.<br /><br />

			Nossos produtos são fabricados com matéria prima de excelente qualidade oferecendo assim a máxima resistência ao produto. Além de tudo possuímos uma equipe técnica capacitada para prestação de serviços como: reforma, assistência técnica e manutenção de parques infantis.
			</p>
		</div>
		<div class="col-6">
			<img src="{{{ asset('/img/foto-empresa.png')}}}" alt="Empresa">
		</div>
	</div>
</div>
<a name="mais"></a>
<div class="container-fluid section-dest">
	<div class="container ">
		<div class="destaques">
			<div class="row">
				<div class="col">
					<span class="quali"></span>
					<h2 class="verde">Qualidade</h2>
					<p>Prezamos sempre pela qualidade para levar mais diversão ao seu espaço</p>
				</div>
				<div class="col">
					<span class="diver"></span>
					<h2 class="vermelho">Diversão</h2>
					<p>A diversão é garantida, com brinquedos que vão trazer a verdadeira essência de brincar</p>
				</div>
				<div class="col">
					<span class="segu"></span>
					<h2 class="azul">Segurança</h2>
					<p>Segurança em cada detalhe, para a brincadeira ser sempre um momento de felicidade</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid prev-int-produtos">
	<div class="container">
		<div class="row">
			<div class="col-5 col-sm-6 col-md-4 col-lg-4 col-xl-5 d-none d-sm-none d-md-block">
				<img src="{{{ asset('/img/prev-inter-produtos.png')}}}" alt="Produtos">
			</div>
			<div class="col-6 col col-sm-6 col-md-4 col-lg-4 col-xl-4 ">
				<p>
					Não fique de fora da
				</p>
				<h4>Diversão</h4>
			</div>
			<div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-3">
				<a href="{{{ url('/#produtos') }}}">Ver Produtos<i class="fa fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid form-contato">
	<div class="container ">
		<div class="row">
			<div class="col col-md-12 col-lg-4 col-xl-4 mt">
				<h4>
					<span>fale com um</span>
					<span>dos nossos
					atendentes</span>
				</h4>
			</div>
			<div class="col col-md-7 col-lg-4 col-xl-4 mt">
				<ul>
					<li>
						<span>telefone</span>
						<strong>48 3646-6651</strong>
					</li>
					<li>
						<span>whats</span>
						<strong>48 9 9942-7791</strong>
					</li>
					<li>
						<span>e-mail</span>
						<strong>contato@fortbrinq.com.br</strong>
					</li>
				</ul>
			</div>
			<div class="col col-md-5 col-lg-4 col-xl-4 d-none d-sm-none d-md-block ">
				<img src="{{{ asset('/img/atendente.png')}}}" alt="Entre em contato">
			</div>
		</div>
	</div>
</div>
@endsection