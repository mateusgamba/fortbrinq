<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<title>Fortbrinq - Parques Infantis</title>
		<meta charset="UTF-8">
		<meta name="description" content="Parques Infantis">
		<meta name="keywords" content="parques, infantis">
		<meta name="author" content="EGG Criativo">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('css/font.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" />
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
	</head>
	<body>
		<header>
			@include('includes.header')
		</header>
    <main>
      @yield('main')
    </main>
		<footer>
			@include('includes.footer')
		</footer>
		<script src="{{ asset('js/toggle.js') }}"></script>
		<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('js/jquery.cookie.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>
		<script>
			function generateTable(hash, row) {
				return `
					<tr>
						<td>
							<a href="{{{ url('/produtos') }}}/${row.product.category.slug}/${row.product.slug}">
								<img src="${row.product.photo_path}" class="img-fluid">
							</a>
						</td>
						<td>
							${row.product.name}
							<br/>
							<small>Categoria: ${row.product.category.name}</small>
						</td>
						<td align="center">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text btnUpdateProduct">
										<a href="javascript:void(0)" onclick="updateProduct('${hash}', ${row.id}, 0)" id="btnLess-${row.id}"><i class="fa fa-minus"></i></a>
									</span>
								</div>
								<input type="text" class="form-control form-control-sm txtUpdateProduct" id="txtProductAmount-${row.id}" value="${row.amount}" readonly="readonly">
								<div class="input-group-append">
									<span class="input-group-text btnUpdateProduct">
										<a href="javascript:void(0)" onclick="updateProduct('${hash}', ${row.id}, 1)"><i class="fa fa-plus"></i></a>
									</span>
								</div>
							</div>
						</td>
						<td align="center">
							<div class="input-group">
								<a class="btn btn-link icon-remove" href="javascript:void(0)" onclick="deleteProduct('${hash}', ${row.id})" title="Excluir produto">
									<i class="fa fa-trash"></i>
								</a>
							</div>
						</td>
					</tr>
				`
			}

			function deleteProduct(hash, id) {
				if (confirm("Você tem certeza que deseja excluir o produto?")) {
					location.href = "{{{ url('/carrinho') }}}/excluir/" + hash + "/" + id;
				}
			}

			function updateProduct(hash, id, value) {
				var product = {
					"hash": hash,
					"id": id,
					"value": value
				};

				$.ajax({
					url: "{{ env('APP_URL') }}/api/cart",
					data: JSON.stringify(product),
					headers: {
						"Content-Type": "application/json",
					},
					processData: false,
					dataType: 'json',
					type: 'PUT',
					success: function(data) {
						$( "#txtProductAmount-"+id ).val(data.data.amount);
						var total = parseInt($( "#totalProducts" ).html());
						var newTotal = value ? total + 1 : total - 1;
						updateLink(newTotal);
						if (data.data.amount === 0) {
							$( "#btnLess-" + id ).hide();
						} else {
							$( "#btnLess-" + id ).show();
						}
					},
					error: function(data) {
						const error = JSON.parse(data.responseText);
						console.log(error);
					}
				});
			}

			function updateLink(total) {
				if ($( "#totalProductsHeader" )[0]) {
					if (total) {
						$( "#totalProductsHeader" ).html(" (" + total + ")").show();
					} else  {
						$( "#totalProductsHeader" ).hide();
					}

				}
				if ($( "#totalProducts" )[0]) {
					$( "#totalProducts" ).html(total);
				}
			}

			$(function() {
				// add product into the basket
				$("#btnAddProduct").click(function() {
					var id = $(this).data("id")
					var products = $.cookie("fortbrinq-products");
					var hash = products === undefined ? Math.floor(Math.random() * 65536) : products ;
					$.cookie("fortbrinq-products", hash, { expires: 1, path: '/' });

					var product = {
						"hash": hash,
						"product_id": id
					};

					$.ajax({
						url: "{{ env('APP_URL') }}/api/cart",
						data: JSON.stringify(product),
						headers: {
							"Content-Type": "application/json",
						},
						processData: false,
						dataType: 'json',
						type: 'POST',
						success: function(data) {
							window.location.href = "{{ env('APP_URL') }}/carrinho";
						},
						error: function(data) {
							const error = JSON.parse(data.responseText);
							console.log(error);
						}
					});
				});

				// List products
				if ($("#listProducts")[0]) {
					var products = $.cookie("fortbrinq-products");

					if (products !== undefined) {
						$( "#hash_products" ).val(products);
						$.ajax({
							url: "{{ env('APP_URL') }}/api/cart/" + products,
							headers: {
								"Content-Type": "application/json",
							},
							processData: false,
							dataType: 'json',
							type: 'GET',
							success: function(response) {
								if (response.data.length) {
									$( ".noProducts" ).hide()
									$( ".hasProducts" ).show();
									var total = 0;
									for(var i=0;i<response.data.length;i++) {
										$('#productsTable').append(generateTable(products, response.data[i]));
										total += response.data[i].amount;
										if (response.data[i].amount===0) {
											$( "#btnLess-" + response.data[i].id ).hide();
										}
									}
									//$( "#totalProducts" ).html(total);
									updateLink(total);
								} else {
									$( ".noProducts" ).show()
									$( ".hasProducts" ).hide();
								}
							},
							error: function(data) {
								const error = JSON.parse(data.responseText);
								console.log(error);
							}
						});
					} else {
						$( ".noProducts" ).show()
						$( ".hasProducts" ).hide();
					}
				}

				if ($("#orderSent")[0]) {
					$.removeCookie("fortbrinq-products", { path: '/' });
				}

				$( "#btnOpenOrcamento" ).click(function() {
					$( "#formOrcamento" ).toggle();
				});

				// Get total
				if (!$("#listProducts")[0]) {
					var products = $.cookie("fortbrinq-products");

					if (products !== undefined) {
						$( "#hash_products" ).val(products);
						$.ajax({
							url: "{{ env('APP_URL') }}/api/cart/" + products + "/total",
							headers: {
								"Content-Type": "application/json",
							},
							processData: false,
							dataType: 'json',
							type: 'GET',
							success: function(response) {
								if (response.data.total) {
									updateLink(response.data.total);
								}
							},
							error: function(data) {
								const error = JSON.parse(data.responseText);
								console.log(error);
							}
						});
					}
				}
			});
		</script>
	</body>
</html>
