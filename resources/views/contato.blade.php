@extends('master')

@section('main')

<div class="container-fluid title-int">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>Contato</h2>
			</div>
			<div class="col">
				<div class="phone float-right">
					<i class="fa fa-phone "></i>
					<span>
						Fale conosco<br />
						<strong>(48) 3646-6651 </strong>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container box-contato mt-5">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 offset-md-2">

			<h3 class="title-contact">ENTRE EM CONTATO</h3>

			<p class="font-default text-center">
				Solicite um orçamento!! É Rápido e Fácil!
				<br />
				Um dos nossos atendentes já entará em contato com você ;D
			</p>

			@if ($message = Session::get('success'))
			<p class="alert alert-success">
				<span class="font-default">Sua mensagem foi enviada com sucesso!</span>
			</p>
			@endif

			<form method="post" action="/contato/enviar" enctype="multipart/form-data" class="mt-5 mb-4">
        {{ csrf_field() }}
				<div class="form-row">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('nome')) is-invalid @endif" placeholder="Nome" name="nome" value="{{ old('nome') }}">
						@if ($errors->has('nome')) <div class="invalid-feedback">{{ $errors->first('nome') }}</div> @endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('email')) is-invalid @endif" placeholder="E-mail" name="email" value="{{ old('email') }}">
						@if ($errors->has('email')) <div class="invalid-feedback">{{ $errors->first('email') }}</div> @endif
					</div>
				</div>
				<div class="form-row">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('telefone')) is-invalid @endif" placeholder="Telefone" name="telefone" value="{{ old('telefone') }}">
						@if ($errors->has('telefone')) <div class="invalid-feedback">{{ $errors->first('telefone') }}</div> @endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('cidade')) is-invalid @endif" placeholder="Cidades" name="cidade" value="{{ old('cidade') }}">
						@if ($errors->has('cidade')) <div class="invalid-feedback">{{ $errors->first('cidade') }}</div> @endif
					</div>
				</div>
				<div class="form-row">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('estado')) is-invalid @endif" placeholder="Estado" name="estado" value="{{ old('estado') }}">
						@if ($errors->has('estado')) <div class="invalid-feedback">{{ $errors->first('estado') }}</div> @endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<select class="form-control @if ($errors->has('perfil')) is-invalid @endif" name="perfil">
							<option value="" selected>Qual o seu perfil?</option>
							<option value="sindico" {{ old('perfil') === 'sindico' ? 'selected' : '' }}>Síndico</option>
							<option value="construtora" {{ old('perfil') === 'construtora' ? 'selected' : '' }}>Construtora</option>
							<option value="escola-creche"{{ old('perfil') === 'escola-creche' ? 'selected' : '' }}>Escola/Creche</option>
							<option value="cliente-final"{{ old('perfil') === 'cliente-final' ? 'selected' : '' }}>Cliente final</option>
						</select>
						@if ($errors->has('perfil')) <div class="invalid-feedback">{{ $errors->first('perfil') }}</div> @endif
					</div>
				</div>
				<div class="form-group">
					<textarea class="form-control @if ($errors->has('mensagem')) is-invalid @endif" rows="3" placeholder="Mensagem" name="mensagem">{{ old('mensagem') }}</textarea>
					@if ($errors->has('mensagem')) <div class="invalid-feedback">{{ $errors->first('mensagem') }}</div> @endif
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-primary mb-2 btn-send">Enviar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="container-fluid g_maps">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14035.193982171122!2d-48.9314177!3d-28.4253365!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x79d0ef10cd5c6250!2sFortBrinq%20Parques%20Infantis%20-%20Disneyl%C3%A2ndia!5e0!3m2!1spt-BR!2sbr!4v1574453099803!5m2!1spt-BR!2sbr" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
<div class="container-fluid prev-int-produtos">
	<div class="container">
		<div class="row">
			<div class="col-5 col-sm-6 col-md-4 col-lg-4 col-xl-5 d-none d-sm-none d-md-block">
				<img src="{{{ asset('/img/prev-inter-produtos.png')}}}" alt="Produtos">
			</div>
			<div class="col-6 col col-sm-6 col-md-4 col-lg-4 col-xl-4 ">
				<p>
					Não fique de fora da
				</p>
				<h4>Diversão</h4>
			</div>
			<div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-3">
				<a href="{{{ url('/#produtos') }}}">Ver Produtos<i class="fa fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</div>
@include('includes.fale-com')
@endsection