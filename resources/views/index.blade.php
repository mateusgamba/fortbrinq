@extends('master')

@section('main')
<div class="container-fluid banner-fundo">
	<div class="container p-relative">
		<div class="row">
			<div class="col-sm-12">
				<div class="phone phone-posi">
					<i class="fa fa-phone"></i>
					<span>
						Fale conosco<br />
						<strong>(48) 3646-6651 </strong>
					</span>
				</div>
			</div>
			<div class="homebotao col-md-6 col-sm-6">
				<span class="text vermelho">
					O seu espaço<br />
					com muito mais<br />
					<strong>diversão</strong>
				</span>
				<div>
					<a href="{{{ url('#produtos') }}}" class="btn-banner btn-a"><i class="fas fa-phone-volume"></i>VER PRODUTOS</a>
					<a href="{{{ url('/contato') }}}" class="btn-banner btn-b">FAZER ORÇAMENTO</a>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 destaque-img">
				<img src="{{{ asset('/img/destaque_banner.png')}}}" alt="Destaque">
			</div>
		</div>
	</div>
</div>
<div class="container-fluid section-dest home-dest">
	<div class="container ">
		<div class="destaques">
			<div class="row">
				<div class="col">
					<span class="quali"></span>
					<h2 class="verde">Qualidade</h2>
					<p>Prezamos sempre pela qualidade para levar mais diversão ao seu espaço</p>
				</div>
				<div class="col">
					<span class="diver"></span>
					<h2 class="vermelho">Diversão</h2>
					<p>A diversão é garantida, com brinquedos que vão trazer a verdadeira essência de brincar</p>
				</div>
				<div class="col">
					<span class="segu"></span>
					<h2 class="azul">Segurança</h2>
					<p>Segurança em cada detalhe, para a brincadeira ser sempre um momento de felicidade</p>
				</div>
			</div>
		</div>
		<div class="orcamento">
			<div class="row">
				<div class="col-12 col-xl-5 col-sm-12 col-md-6">
					<h3>
						Solicite um <br />
						<strong>orçamento</strong><br />
						<span>rápio e fácil</span>
					</h3>
					<span class="btn">
						<i class="fa fa-phone"></i>
						<span>(48) 3646-6651 </span>
					</span>
					<span class="btn">
						<i class="fa fa-envelope"></i>
						<span>contato@fortbrinq.com.br</span>
					</span>
				</div>
				<div class="col-12 col-xl-7 col-sm-12 col-md-6">
					<img src="{{{ asset('/img/banner_orca.png')}}}" alt="Orçamento">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid box-produto">
	<div class="container">
		<div class="title">
			<a name="produtos"></a>
			<h3>Produtos</h3>
		</div>
		<div class="row">
			@foreach ($categories as $category)
				@php
					$name = $category->name;
					$position = strpos($name, ' ');
					$nameTitle = substr($name, 0, $position);
					$description = substr($name, $position + 1);
				@endphp
			<div class=" col-xl-4 col-lg-4 col-md-6 col-sm-12">
				<a href="{{{ url('/produtos') }}}/{{{$category->slug}}}">
					<div class="item">
						<div class="box-img">
							<img src="{{{ $category->image_path }}}">
						</div>
						<div class="box-text">
							<h5>
								{{{ $nameTitle }}}
								<span>{{{ $description }}}</span>
							</h5>
							<i class="fa fa-chevron-right"></i>
						</div>
					</div>
				</a>
			</div>
			@endforeach
			<div class=" col-xl-4 col-lg-4 col-md-6 col-sm-12">
			<a href="{{{ url('/produtos') }}}">
					<div class="item-chamada">
						confira todos<br />
						os nossos<br />
						<span>produtos</span><br/>
						<i class="fa fa-chevron-right"></i>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid prev-empresa">
	<div class="container">
		<div class="row">
			<div class="col col-sm-6 col-md-4 col-lg-4 col-xl-4 d-none d-sm-none d-md-block">
				<img src="{{{ asset('/img/empresa.png')}}}" alt="Empresa">
			</div>
			<div class="col col col-sm-6 col-md-4 col-lg-4 col-xl-4 ">
				<h4>sobre nós</h4>
				<p>
					desde 1980 levando<br />
					diversão as famílias
				</p>
			</div>
			<div class="col col-sm-6 col-md-4 col-lg-4 col-xl-4">
				<a href="{{{ url('/sobre') }}}">Ver História<i class="fa fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</div>
@include('includes.fale-com')
@endsection
