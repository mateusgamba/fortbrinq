@extends('master')

@section('main')
<div class="container-fluid title-int">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>Carrinho</h2>
				<span class="breadcrumbs">
					<a href="{{{ url('/') }}}">Home</a>
					/
					Carrinho
				</span>
			</div>
			<div class="col">
				<div class="phone float-right">
					<i class="fa fa-phone"></i>
					<span>
						Fale conosco<br />
						<strong>(48) 3646-6651 </strong>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="container">

		<div class="row">
			<div class="col-12">
				<h2 class="title-produto-individual-subtitles">Meu carrinho</h2>

				<div class="alert alert-warning font-tahoma noProducts" style="display:none">
						Não há Produtos em seu carrinho.
				</div>

				<div id="listProducts" class="hasProducts" style="display:none">
					<table style="width:100%;" class="table">
						<thead>
							<tr>
								<th style="width:10%" class="border-top-0">Produto</th>
								<th class="border-top-0">&nbsp;</th>
								<th style="width:15%" class="text-center border-top-0">Quantidade</th>
								<th style="width:5%" class="text-center border-top-0">&nbsp;</th>
							</tr>
						</thead>
						<tbody id="productsTable"></tbody>
						<tfoot>
							<tr>
								<th colspan="2">Quantidade total:</th>
								<th id="totalProducts" class="text-center"></th>
								<th>&nbsp;</th>
							</tr>
						</tfoot>
					</table>
				</div>

				<div class="row" style="height:80px">
					<div class="col-12">
						<div class="text-center mt-3">
							<a href="{{{ url('/#produtos') }}}" class="btn-produto btn-a">Adicionar mais Produtos</a>
							<a href="javascript:void(0)" id="btnOpenOrcamento" class="btn-produto btn-a hasProducts" style="display:none">Enviar Or&ccedil;amento</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-8 offset-md-2" style="display: @if (session('enableOrcamento')) block @else none @endif" id="formOrcamento">
			<h2 class="title-produto-individual-subtitles">Preencha o formul&aacute;rio abaixo para enviar seu or&ccedil;amento.</h2>

			<form method="post" action="{{{ url('/carrinho/enviar') }}}" enctype="multipart/form-data" class="mb-4">
				{{ csrf_field() }}
				<div class="form-row">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('nome')) is-invalid @endif" placeholder="Nome" name="nome" value="{{ old('nome') }}">
						@if ($errors->has('nome')) <div class="invalid-feedback">{{ $errors->first('nome') }}</div> @endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('email')) is-invalid @endif" placeholder="E-mail" name="email" value="{{ old('email') }}">
						@if ($errors->has('email')) <div class="invalid-feedback">{{ $errors->first('email') }}</div> @endif
					</div>
				</div>
				<div class="form-row">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('telefone')) is-invalid @endif" placeholder="Telefone" name="telefone" value="{{ old('telefone') }}">
						@if ($errors->has('telefone')) <div class="invalid-feedback">{{ $errors->first('telefone') }}</div> @endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('cidade')) is-invalid @endif" placeholder="Cidades" name="cidade" value="{{ old('cidade') }}">
						@if ($errors->has('cidade')) <div class="invalid-feedback">{{ $errors->first('cidade') }}</div> @endif
					</div>
				</div>
				<div class="form-row">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<input type="text" class="form-control @if ($errors->has('estado')) is-invalid @endif" placeholder="Estado" name="estado" value="{{ old('estado') }}">
						@if ($errors->has('estado')) <div class="invalid-feedback">{{ $errors->first('estado') }}</div> @endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<select class="form-control @if ($errors->has('perfil')) is-invalid @endif" name="perfil">
							<option value="" selected>Qual o seu perfil?</option>
							<option value="sindico" {{ old('perfil') === 'sindico' ? 'selected' : '' }}>Síndico</option>
							<option value="construtora" {{ old('perfil') === 'construtora' ? 'selected' : '' }}>Construtora</option>
							<option value="escola-creche"{{ old('perfil') === 'escola-creche' ? 'selected' : '' }}>Escola/Creche</option>
							<option value="cliente-final"{{ old('perfil') === 'cliente-final' ? 'selected' : '' }}>Cliente final</option>
						</select>
						@if ($errors->has('perfil')) <div class="invalid-feedback">{{ $errors->first('perfil') }}</div> @endif
					</div>
				</div>
				<div class="form-group">
					<textarea class="form-control @if ($errors->has('mensagem')) is-invalid @endif" rows="3" placeholder="Mensagem" name="mensagem">{{ old('mensagem') }}</textarea>
					@if ($errors->has('mensagem')) <div class="invalid-feedback">{{ $errors->first('mensagem') }}</div> @endif
				</div>
				<div class="form-group">
					<input type="text" class="form-control d-none" name="hash_products" id="hash_products"/>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-primary mb-2 btn-send">Enviar</button>
				</div>
			</form>

    </div>
	</div>
</div>
@endsection