<div class="listra"></div>
<div class="container content-top">
	<a href="{{{ url('/') }}}" class="logo">
		<img src="{{{ asset('/img/logo.png')}}}" alt="Fortbrinq">
	</a>

	<div class="searchnav">
		<div class="group-seart">
			<div class="action" style="height:35px">
				<a href="https://www.instagram.com/brubrinq/" target="_blank" class="icon insta">
					<i class="fa fa-instagram"></i>
				</a>
				<a href="" class="icon face"><i class="fa fa-facebook-f"></i></a>
				<form role="search" method="get" class="search-form" active="{{{ url('/produtos') }}}">
					<div class="icon b-icon search">
						<input class="search-field" placeholder="Pesquisar" value="" name="s" title="Search for:">
						<a class="btn-search">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="menu">
		<nav class="nav" id="navbar">
			<a href="javascript:void(0)" class="botao_responsive" id="botao_responsive">
				<i class="fa fa-bars"></i>
			</a>
			<a href="{{{ url('/') }}}" class="laranja">Home</a>
			<a href="{{{ url('/sobre') }}}" class="vermelho">Sobre nós</a>
			<a href="{{{ url('/#produtos') }}}" class="azul">Produtos</a>
			<!-- <a href="obras.html" class="amarelo">Obras Realizadas</a> -->
			<a href="{{{ url('/carrinho') }}}" class="amarelo">Carrinho <span class="font-tahoma" style="font-size:14px" id="totalProductsHeader"></span></a>
			<a href="{{{ url('/contato') }}}" class="verde">Contato</a>
		</nav>
	</div>
	<div class="line"></div>
</div>