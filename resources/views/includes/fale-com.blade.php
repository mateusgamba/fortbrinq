<div class="container-fluid form-contato">
	<div class="container ">
		<div class="row">
			<div class="col col-md-12 col-lg-4 col-xl-4 mt">
				<h4>
					<span>fale com um</span>
					<span>dos nossos
					atendentes</span>
				</h4>
			</div>
			<div class="col col-md-7 col-lg-4 col-xl-4 mt">
				<ul>
					<li>
						<span>telefone</span>
						<strong>48 3646-6651</strong>
					</li>
					<li>
						<span>whats</span>
						<strong>48 9 9942-7791</strong>
					</li>
					<li>
						<span>e-mail</span>
						<strong>contato@fortbrinq.com.br</strong>
					</li>
				</ul>
			</div>
			<div class="col col-md-5 col-lg-4 col-xl-4 d-none d-sm-none d-md-block ">
        <img src="{{{ asset('/img/atendente.png')}}}" alt="Entre e contato">
			</div>
		</div>
	</div>
</div>