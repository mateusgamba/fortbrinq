@php
	$categories = App\Category::getCategories();
@endphp
<a class="box-whats" href="https://wa.me/5548996110393" target="_black">
	<i class="fa fa-whatsapp"></i><span>Chamar no Whats</span>
</a>

<div class="topo_footer">
	<div class="container">
		<a href="{{{ url('/') }}}">
			<img src="{{{ asset('/img/logo_footer.png')}}}" alt="Fortbrinq">
		</a>
		<div class="social">
			<a href="https://www.instagram.com/fortbrinq/" target="_blank" class="icon insta">
				<i class="fa fa-instagram"></i>
			</a>
			<a href="https://www.instagram.com/fortbrinq/" class="icon face"><i class="fa fa-facebook-f"></i></a>
			<a href="https://wa.me/5548996110393" class="icon whats"><i class="fa fa-whatsapp"></i></a>
		</div>
	</div>
</div>
<div class="corpo_footer">
	<div class="container">
		<div class="row">
			<div class="col">
				<h3><a href="{{{ url('/') }}}">Home</a></h3>
				<ul>
					<li></li>
				</ul>
			</div>
			<div class="col">
				<h3><a href="{{{ url('/sobre') }}}">Sobre nós</a></h3>
				<ul>
					<li><a href="{{{ url('/sobre#historia') }}}">História</a></li>
					<li><a href="{{{ url('/sobre#mais') }}}">Missão</a></li>
					<li><a href="{{{ url('/sobre#mais') }}}">Visão</a></li>
					<li><a href="{{{ url('/sobre#mais') }}}">Valores</a></li>
				</ul>
			</div>
			<div class="col">
				<h3><a href="{{{ url('/#produtos') }}}">Produtos</a></h3>
				<ul>
					@foreach ($categories as $category)
						<li><a href="{{{ url('/produtos') }}}/{{{$category->slug}}}">{{{ $category->name }}}</a></li>
					@endforeach
				</ul>
			</div>
			<!--
			<div class="col">
				<h3><a href="">Obras Realizadas</a></h3>
				<ul>
					<li></li>
				</ul>
			</div>
			-->
			<div class="col">
				<h3><a href="{{{ url('/contato') }}}">Contato</a></h3>
				<ul>
					<li><a href="">Tel: (48) 3646-6651</a></li>
					<li><a href="">contato@fortbrinq.com.br</a></li>
					<li><a href="">Whats: 48 9 9942 7791</a></li>
					<li><a href="">BR 101 km 323<br />
									Estiva - Pescaria Brava - SC
									CEP: 88790-000.
								</a></li>
				</ul>
			</div>
		<div class="line"></div>
		</div>
	</div>
</div>
<div class="assina">
	<div class="container">
		<div class="row">
			<div class="col">
				<span>Copyright. Todos os direitos reservados a FORTBRINQ. © 2019</span>
			</div>
			<div class="col text-right">
				<a href="http://eggcriativo.com/" target="_black">
					<img src="{{{ asset('/img/egg_criativo.png')}}}" alt="EGG Criativo">
				</a>
			</div>
		</div>
	</div>
</div>