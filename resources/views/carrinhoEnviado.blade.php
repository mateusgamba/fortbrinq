@extends('master')

@section('main')
<div class="container-fluid title-int">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>Carrinho</h2>
				<span class="breadcrumbs">
					<a href="{{{ url('/') }}}">Home</a>
					/
					Enviado com sucesso
				</span>
			</div>
			<div class="col">
				<div class="phone float-right">
					<i class="fa fa-phone "></i>
					<span>
						Fale conosco<br />
						<strong>(48) 3646-6651 </strong>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid mt-3" id="orderSent">
	<div class="container" style="height:180px">

		<div class="row">
			<div class="col-12">
				<h2 class="title-produto-individual-subtitles text-center">Seu orçamento foi enviado com sucesso, em breve entraremos em contato.</h2>
				<br />
				<div class="text-center mt-3">
					<a href="{{{ url('/') }}}" class="btn-produto btn-a">Continui navegando pelo site</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection