@extends('master')

@section('main')
<div class="container-fluid title-int">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>Produtos</h2>
				<span class="breadcrumbs">
					<a href="{{{ url('/') }}}">Home</a>
					/
					<a href="{{{ url('/#produtos') }}}">Produtos</a>
					/
					{{{ $breadcrumb }}}
				</span>
			</div>
			<div class="col">
				<div class="phone float-right">
					<i class="fa fa-phone "></i>
					<span>
						Fale conosco<br />
						<strong>(48) 3646-6651 </strong>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid box-produto">
	<div class="container">
		@foreach ($categories as $category)
			<div class="title mt-0">
				<h3>{{{$category->name}}}</h3>
			</div>
			<div class="row">
				@foreach ($category->products as $product)
					@php
						if (empty($product->photo)) {
                            $photo = '/img/produto-sem-imagem.gif';
						} else {
                            $photo = $product->photo_path;
						}
						$slugCategory = $breadcrumb === 'Todos' ? 'todos' : $category->slug;
					@endphp

					<div class=" col-xl-3 col-lg-3 col-md-6 col-sm-12">
						<a href="{{{ url('/produtos') }}}/{{{ $slugCategory }}}/{{{ $product->slug }}}">
							<div class="item">
								<div class="box-img box-img-list">
									<img src="{{{ $photo }}}" alt="{{{ $product->name }}}">
								</div>
								<div class="box-text-individual">
									<h5 class="font-item-produto">
										{{{ $product->name }}}
									</h5>
									<i class="fa fa-chevron-right icon-item-font"></i>
								</div>
							</div>
						</a>

					</div>
					@endforeach
				</div>
		@endforeach
	</div>
</div>
@endsection
