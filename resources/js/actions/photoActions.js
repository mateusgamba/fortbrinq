import axiosInstance from "./axiosInstance";

import {
    GET_PHOTOS,
    DELETE_PHOTO,
    GET_ERRORS,
    ADD_PHOTO,
    CLEAR_ERRORS,
    SAVE_FALSE,
    EDIT_FALSE,
    UPDATE_PHOTO
} from "./types";

// Get Categories
export const getPhotos = id => dispatch => {
    axiosInstance
        .get("/api/products/" + id + "/photos")
        .then(res =>
            dispatch({
                type: GET_PHOTOS,
                payload: res.data.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PHOTOS,
                payload: null
            })
        );
};

export const deletePhoto = (idProduct, idPhoto) => dispatch => {
    axiosInstance
        .delete("/api/products/" + idProduct + "/photos/" + idPhoto)
        .then(res =>
            dispatch({
                type: DELETE_PHOTO,
                payload: idPhoto
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
};

export const savePhoto = (idProduct, data) => dispatch => {
    dispatch(clearErrors());
    axiosInstance
        .post("/api/products/" + idProduct + "/photos", data)
        .then(res => {
            dispatch({
                type: ADD_PHOTO,
                payload: res.data.data
            });
            dispatch({
                type: SAVE_FALSE
            });
            // dispatch(setMessage(res.data.message));
            // dispatch(setMessage(""));
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
};

export const updatePhoto = (idProduct, id, data) => dispatch => {
    dispatch(clearErrors());
    axiosInstance
        .put("/api/products/" + idProduct + "/photos/" + id, data)
        .then(res => {
            dispatch({
                type: UPDATE_PHOTO,
                payload: res.data.data
            });

            dispatch({
                type: EDIT_FALSE
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response
            });
        });
};

export const clearErrors = () => {
    return {
        type: CLEAR_ERRORS
    };
};
