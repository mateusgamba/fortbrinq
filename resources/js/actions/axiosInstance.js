const axios = require("axios");

const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_URL,
    headers: { "Content-Type": "application/json", Accept: "application/json" }
});

axiosInstance.interceptors.request.use(
    config => {
        const auth = localStorage.getItem("incomap-auth");
        if (auth) {
            const authParse = JSON.parse(auth);
            config.headers.common["Authorization"] = `${authParse.type} ${
                authParse.token
            }`;
        }
        return config;
    },
    function(error) {
        return Promise.reject(error);
    }
);

axiosInstance.interceptors.response.use(
    response => {
        if (response.headers.authorization) {
            const auth = response.headers.authorization.split(" ");
            const data = {
                type: auth[0],
                token: auth[1]
            };
            localStorage.setItem("incomap-auth", JSON.stringify(data));
        }

        return response;
    },
    function(error) {
        return Promise.reject(error);
    }
);

module.exports = axiosInstance;
