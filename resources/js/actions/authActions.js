import axiosInstance from "./axiosInstance";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

import { GET_ERRORS, SET_CURRENT_USER, CLEAR_ERRORS } from "./types";

// Login - Get User Token
export const loginUser = user => dispatch => {
    dispatch(clearErrors());
    axiosInstance
        .post("/api/auth/login", user)
        .then(response => {
            const { type, token, expires_in } = response.data.data;
            setAuthToken(type, token, expires_in);
            const userDecode = jwt_decode(token);
            dispatch(setCurrentUser(userDecode));
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
};

// Set logged in user
export const setCurrentUser = decoded => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    };
};

// Log user out
export const logoutUser = () => dispatch => {
    setAuthToken(false);
    dispatch(setCurrentUser({}));
};

// Clear errors
export const clearErrors = () => {
    return {
        type: CLEAR_ERRORS
    };
};
