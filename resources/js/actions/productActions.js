import axiosInstance from "./axiosInstance";
import {
    ADD_PRODUCT,
    OPEN_ADD_PRODUCT_MODAL,
    CLOSE_ADD_PRODUCT_MODAL,
    GET_ERRORS,
    CLEAR_ERRORS,
    DELETE_PRODUCT,
    GET_PRODUCTS,
    GET_PRODUCT,
    TOGGLE_TAB,
    GET_CATEGORY,
    SAVE_FALSE,
    EDIT_PRODUCT
} from "./types";
import { getCategory } from './categoryActions';

export const getProductsByCategory = id => dispatch => {
    axiosInstance
        .get("/api/categories/" + id + "/products")
        .then(res =>
            dispatch({
                type: GET_PRODUCTS,
                payload: res.data.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PRODUCTS,
                payload: null
            })
        );
};

// Save Product
export const saveProduct = (data, slug) => dispatch => {
    dispatch(clearErrors());

    console.log({data});
    axiosInstance
        .post("/api/products", data)
        .then(res => {
            dispatch({
                type: ADD_PRODUCT,
                payload: res.data.data
            });

            dispatch({
                type: SAVE_FALSE
            });
            dispatch(getCategory(res.data.slug));
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response
            });
        });
};

// edit Product
export const editProduct = (id, data) => dispatch => {
    dispatch(clearErrors());
    axiosInstance
        .post(`/api/products-update/${id}`, data)
        .then(res => {
            dispatch({
                type: EDIT_PRODUCT,
                payload: res.data.data
            });

            dispatch({
                type: SAVE_FALSE
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response
            });
        });
};

export const openAddProductModal = () => dispatch => {
    dispatch({
        type: OPEN_ADD_PRODUCT_MODAL,
        payload: true
    });
};

export const closeAddProductModal = () => dispatch => {
    dispatch(clearErrors());

    dispatch({
        type: CLOSE_ADD_PRODUCT_MODAL,
        payload: false
    });
};

export const clearErrors = () => {
    return {
        type: CLEAR_ERRORS
    };
};

export const deleteProduct = id => dispatch => {
    axiosInstance
        .delete("/api/products/" + id)
        .then(res =>
            dispatch({
                type: DELETE_PRODUCT,
                payload: res.data.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
};

export const toggleTab = tab => dispatch => {
    dispatch({
        type: TOGGLE_TAB,
        payload: tab
    });
};

// Save Product
export const getProduct = id => dispatch => {
    dispatch(clearErrors());
    dispatch({
        type: GET_PRODUCT,
        payload: {}
    });
    const headers = {
        "Content-Type": "application/json",
        Accept: "application/json"
    };
    axiosInstance
        .get(`/api/product/${id}`, { headers })
        .then(res => {
            dispatch({
                type: GET_PRODUCT,
                payload: res.data.data
            });
        })
        .catch(err => {
            dispatch({
                type: GET_PRODUCT,
                payload: null
            });
        });
};

export const getByCategory = id => dispatch => {
    axiosInstance
        .get("/api/products/byCategories/" + id)
        .then(res =>
            dispatch({
                type: GET_PRODUCTS,
                payload: res.data.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PRODUCTS,
                payload: null
            })
        );
};
