import axiosInstance from "./axiosInstance";
import {
    GET_CATEGORIES,
    GET_CATEGORY,
    ADD_CATEGORY,
    SAVE_FALSE,
    GET_ERRORS,
    CLEAR_ERRORS,
    UPDATE_CATEGORY,
 } from "./types";

// Get Categories
export const getCategories = () => dispatch => {
    axiosInstance
        .get("/api/categories")
        .then(res =>
            dispatch({
                type: GET_CATEGORIES,
                payload: res.data.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_CATEGORIES,
                payload: null
            })
        );
};

// Get Category
export const getCategory = id => dispatch => {
    axiosInstance
        .get(`/api/categories/${id}/products`)
        .then(res =>
            dispatch({
                type: GET_CATEGORY,
                payload: res.data.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_CATEGORY,
                payload: null
            })
        );
};

// Save Category
export const saveCategory = data => dispatch => {
    dispatch(clearErrors());
    axiosInstance
        .post("/api/categories", data)
        .then(res => {
            dispatch({
                type: ADD_CATEGORY,
                payload: res.data.data
            });

            dispatch({
                type: SAVE_FALSE
            });

            dispatch(getCategories());

        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response
            });
        });
};

// update Category
export const updateCategory = (id, data) => dispatch => {
    dispatch(clearErrors());
    axiosInstance
        .post(`/api/categories-update/${id}`, data)
        .then(res => {
            dispatch({
                type: UPDATE_CATEGORY,
                payload: res.data.data
            });

            dispatch({
                type: SAVE_FALSE
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response
            });
        });
};

export const clearErrors = () => {
    return {
        type: CLEAR_ERRORS
    };
};
