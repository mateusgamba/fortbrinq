export const GET_ERRORS = "GET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const SET_CURRENT_USER = "SET_CURRENT_USER";

// Categories
export const GET_CATEGORIES = "GET_CATEGORIES";
export const GET_CATEGORY = "GET_CATEGORY";
export const ADD_CATEGORY = "ADD_CATEGORY";
export const UPDATE_CATEGORY = "UPDATE_CATEGORY";
export const DELETE_CATEGORY = "DELETE_CATEGORY";

// Products
export const ADD_PRODUCT = "ADD_PRODUCT";
export const OPEN_ADD_PRODUCT_MODAL = "OPEN_ADD_PRODUCT_MODAL";
export const CLOSE_ADD_PRODUCT_MODAL = "CLOSE_ADD_PRODUCT_MODAL";
export const DELETE_PRODUCT = "DELETE_PRODUCT";
export const GET_PRODUCTS = "GET_PRODUCTS";
export const GET_PRODUCT = "GET_PRODUCT";
export const EDIT_PRODUCT = "EDIT_PRODUCT";
export const TOGGLE_TAB = "TOGGLE_TAB";
export const SAVE_FALSE = "SAVE_FALSE";

// Photos
export const GET_PHOTOS = "GET_PHOTOS";
export const DELETE_PHOTO = "DELETE_PHOTO";
export const ADD_PHOTO = "ADD_PHOTO";
export const EDIT_FALSE = "EDIT_FALSE";
export const UPDATE_PHOTO = "UPDATE_PHOTO";
