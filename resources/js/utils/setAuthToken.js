const setAuthToken = (type, token, expires_in) => {
    if (token) {
        const data = {
            type,
            token,
            expires_in
        };
        localStorage.setItem("incomap-auth", JSON.stringify(data));
    } else {
        localStorage.removeItem("incomap-auth");
    }
};

export default setAuthToken;
