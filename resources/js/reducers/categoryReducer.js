import {
    GET_CATEGORIES,
    GET_CATEGORY,
    ADD_CATEGORY,
    SAVE_FALSE,
    UPDATE_CATEGORY
} from "../actions/types";

const initialState = {
    categories: [],
    category: {},
    saved: false,
};

export default function(state = initialState, action) {
    switch (action.type) {
        case ADD_CATEGORY:
            return {
                ...state,
                categories: [action.payload, ...state.categories],
                saved: true
            };
        case GET_CATEGORIES:
            return {
                ...state,
                categories: action.payload,
                saved: false
            };
        case GET_CATEGORY:
            return {
                ...state,
                category: action.payload,
                saved: false
            };
        case SAVE_FALSE:
            return {
                ...state,
                saved: false
            };
        case UPDATE_CATEGORY:
            return {
                ...state,
                categories: state.categories.map(category => {
                    return category.id === action.payload.id
                        ? action.payload
                        : category;
                }),
                category: action.payload,
                saved: true
            };
        default:
            return state;
    }
}
