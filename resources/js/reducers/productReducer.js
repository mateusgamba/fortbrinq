import {
    ADD_PRODUCT,
    OPEN_ADD_PRODUCT_MODAL,
    CLOSE_ADD_PRODUCT_MODAL,
    DELETE_PRODUCT,
    GET_PRODUCTS,
    GET_PRODUCT,
    SAVE_FALSE,
    EDIT_PRODUCT
} from "../actions/types";

const initialState = {
    products: [],
    product: {},
    modalProduct: false,
    modalTab: "1",
    saved: false
};

export default function(state = initialState, action) {
    switch (action.type) {
        case ADD_PRODUCT:
            return {
                ...state,
                products: [action.payload, ...state.products],
                product: action.payload,
                saved: true
            };

        case GET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            };
        case GET_PRODUCT:
            return {
                ...state,
                product: action.payload
            };
        case EDIT_PRODUCT:
            return {
                ...state,
                products: state.products.map(product => {
                    return product.id === action.payload.id
                        ? action.payload
                        : product;
                }),
                product: action.payload,
                saved: true
            };
        case DELETE_PRODUCT:
            alert("Excluído com sucesso.");
            return {
                ...state,
                products: state.products.filter(
                    product => product.id !== action.payload.id
                )
            };
        case OPEN_ADD_PRODUCT_MODAL:
            return {
                ...state,
                modalProduct: action.payload,
                modalTab: "1"
            };
        case CLOSE_ADD_PRODUCT_MODAL:
            return {
                ...state,
                modalProduct: action.payload
            };
        case SAVE_FALSE:
            return {
                ...state,
                saved: false
            };

        default:
            return state;
    }
}
