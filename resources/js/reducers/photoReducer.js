import {
    GET_PHOTOS,
    DELETE_PHOTO,
    SAVE_FALSE,
    ADD_PHOTO,
    EDIT_FALSE,
    UPDATE_PHOTO
} from "../actions/types";

const initialState = {
    photos: [],
    saved: false,
    edited: false
};

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_PHOTOS:
            return {
                ...state,
                photos: action.payload
            };
        case DELETE_PHOTO:
            alert("Excluído com sucesso.");
            return {
                ...state,
                photos: state.photos.filter(
                    photo => photo.id !== action.payload
                )
            };
        case ADD_PHOTO:
            return {
                ...state,
                photos: [action.payload, ...state.photos],
                saved: true
            };
        case UPDATE_PHOTO:
            return {
                ...state,
                photos: state.photos.map(photo => {
                    return photo.id === action.payload.id
                        ? action.payload
                        : photo;
                }),
                edited: true
            };
        case SAVE_FALSE:
            return {
                ...state,
                saved: false
            };
        case EDIT_FALSE:
            return {
                ...state,
                edited: false
            };
        default:
            return state;
    }
}
