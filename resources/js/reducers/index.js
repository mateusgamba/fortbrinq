import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import categoryReducer from "./categoryReducer";
import productReducer from "./productReducer";
import photoReducer from "./photoReducer";

export default combineReducers({
    auth: authReducer,
    errors: errorReducer,
    category: categoryReducer,
    product: productReducer,
    photo: photoReducer
});
