import React, { Component } from "react";
import { createHistory, LocationProvider, Router } from "@reach/router";

import { Provider } from "react-redux";
import store from "./store";
import PrivateRoute from "./components/common/PrivateRoute";

import jwt_decode from "jwt-decode";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import Login from "./components/auth/Login";
import Logout from "./components/auth/Logout";
import ProductManagement from "./components/product/ProductManagement";

import createHashSource from "hash-source";

// Check for token
const auth = localStorage.getItem("incomap-auth");
if (auth) {
    const authParse = JSON.parse(auth);
    const decoded = jwt_decode(authParse.token);
    store.dispatch(setCurrentUser(decoded));

    // Check for expired token
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        // Logout user
        store.dispatch(logoutUser());

        const hostname = window.location.hostname;
        const protocol = window.location.protocol;
        const port = window.location.port ? ":" + window.location.port : "";
        window.location.href = protocol + "//" + hostname + port + "/admin";
    }
}

let source = createHashSource();
let history = createHistory(source);

// class ScrollUp extends React.Component {
//     componentDidMount() {
//         requestAnimationFrame(() => {
//             window.scrollTo(0, 0);
//         });
//     }

//     render() {
//         return this.props.children;
//     }
// }

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <LocationProvider history={history}>
                    <div>
                        <div className="container">
                            <Router primary={false}>
                                <Login path="/" />
                                <Logout path="/logout" />
                                <PrivateRoute
                                    path="/product-management/*"
                                    component={ProductManagement}
                                />
                            </Router>
                        </div>
                    </div>
                </LocationProvider>
            </Provider>
        );
    }
}
