import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getCategories } from "../../actions/categoryActions";
import NavLink from "./../common/NavLink";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    Input,
    FormFeedback,
    Alert
} from "reactstrap";

class Categories extends Component {
    componentDidMount() {
        this.props.getCategories();
    }

    render() {
        const { categories } = this.props.category;
        return (
            <div>
                <h4 className="mb-0">Categorias1</h4>
                <div className="list-group mt-3">
                    {categories.map(category => {
                        return (
                            <NavLink
                                to={`/product-management/${
                                    category.slug
                                }/products`}
                                key={category.id}
                            >
                                {category.name}
                            </NavLink>
                        );
                    })}
                </div>
            </div>
        );
    }
}

Categories.propTypes = {
    getCategories: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    category: state.category
});

export default connect(
    mapStateToProps,
    { getCategories }
)(Categories);
