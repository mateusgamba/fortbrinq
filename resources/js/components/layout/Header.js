import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import { Redirect, redirectTo, Link } from "@reach/router";

class Header extends Component {
    constructor() {
        super();
        this.state = {
            auth: {}
        };
    }

    logout() {
        this.props.logoutUser();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.setState({ auth: nextProps.auth.isAuthenticated });
        }
    }

    render() {
        return (
            <header className="blog-header py-2 fixed-top bg-white shadow-sm">
                <div className="container">
                    <div className="row flex-nowrap justify-content-between align-items-center">
                        <div className="col-sm-10 text-left">
                            <h3>Administra&ccedil;&atilde;o Site</h3>
                        </div>

                        <div className="col-sm-2 text-right">
                            <Link
                                to="/logout"
                                className="btn btn-outline-secondary"
                            >
                                Sair
                            </Link>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    logoutUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(
    mapStateToProps,
    { logoutUser }
)(Header);
