import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";
import TextFieldGroup from "../common/TextFieldGroup";
import { navigate, Link, redirectTo, Redirect } from "@reach/router";

class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            errors: {}
        };
        //console.log("login");
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    // componentDidMount() {
    //     if (this.props.auth.isAuthenticated) {
    //         redirectTo("/product-management");
    //     }
    // }

    componentWillReceiveProps(nextProps) {
        //console.log(nextProps.auth);
        if (nextProps.auth.isAuthenticated) {
            //   navigate("#/product-management");
            //redirectTo("/product-management");
        }

        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }

    onSubmit(e) {
        e.preventDefault();

        const userData = {
            email: this.state.email,
            password: this.state.password
        };

        this.props.loginUser(userData);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        const { errors } = this.state;
        const errorsPassword = errors.errors ? errors.errors.password[0] : "";
        const errorsEmail = errors.errors ? errors.errors.email[0] : "";
        //console.log(this.props.auth);
        if (this.props.auth.isAuthenticated) {
            return <Redirect noThrow to="/product-management" />;
        }

        return (
            <div className="login">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">Acesso</h1>
                            {errors.message && (
                                <div
                                    className="alert alert-danger"
                                    role="alert"
                                >
                                    {errors.message}
                                </div>
                            )}
                            <form onSubmit={this.onSubmit} noValidate>
                                <TextFieldGroup
                                    placeholder="Email"
                                    name="email"
                                    type="email"
                                    value={this.state.email}
                                    onChange={this.onChange}
                                    error={errorsEmail}
                                />

                                <TextFieldGroup
                                    placeholder="Senha"
                                    name="password"
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.onChange}
                                    error={errorsPassword}
                                />
                                <input
                                    type="submit"
                                    className="btn btn-info btn-block mt-4"
                                    value="Acessar"
                                />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);
