import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import { Redirect } from "@reach/router";

class Logout extends Component {
    componentDidMount() {
        this.props.logoutUser();
    }

    render() {
        return <Redirect noThrow to="/" />;
    }
}

Logout.propTypes = {
    logoutUser: PropTypes.func.isRequired
};

export default connect(null, { logoutUser })(Logout);
