import React, { Component } from "react";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    Alert,
    FormGroup,
    Label,
    Input,
    FormFeedback
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
    getPhotos,
    deletePhoto,
    savePhoto,
    updatePhoto
} from "../../actions/photoActions";
import isEmpty from "../../utils/isEmpty";
import classnames from "classnames";

class PhotosModal extends Component {
    constructor() {
        super();
        this.state = {
            modal: true,
            nestedModal: false,
            nestedModal1: false,
            errors: {},
            photo: [],
            photo_preview: "",
            editPhoto: 0,
            idUpdate: 0,
            descriptionUpdate: ""
        };

        this.close = this.close.bind(this);
    }

    componentDidMount() {
        this.props.getPhotos(this.props.id);
    }

    deletePhoto(id) {
        if (window.confirm("Você tem certeza que deseja remove essa foto?")) {
            this.props.deletePhoto(this.props.id, id);
        }
    }

    close() {
        this.setState({ modal: false });
        setTimeout(function() {
            window.history.back();
        }, 500);
    }

    toggleNested() {
        this.setState({
            nestedModal: !this.state.nestedModal,
            closeAll: false
        });
    }

    toggleNested1(id, description) {
        this.setState({
            nestedModal1: !this.state.nestedModal1,
            closeAll: false,
            idUpdate: id,
            descriptionUpdate: description
        });
    }

    newPhoto() {
        this.setState({
            nestedModal: !this.state.nestedModal,
            photo_preview: false,
            closeAll: false
        });
    }

    savePhoto() {
        alert("enviar");
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    componentWillReceiveProps(newProps) {
        if (newProps.errors) {
            this.setState({ errors: newProps.errors });
        }

        if (this.props.photo.saved) {
            alert("Salvo com sucesso");
            this.toggleNested();
        }

        if (this.props.photo.edited) {
            alert("Salvo com sucesso");
            this.toggleNested1();
        }
    }

    savePhoto(e) {
        e.preventDefault();

        var formData = new FormData();
        formData.append("photo", this.state.photo[0]);
        formData.append(
            "description",
            this.state.description ? this.state.description : ""
        );

        this.props.savePhoto(this.props.id, formData);
    }

    clearImage() {
        this.setState({ photo: [] });
    }

    onChangePhoto(event) {
        if (event.target.files[0] === undefined) {
            this.clearImage();
            return false;
        }

        const fileType = event.target.files[0].type;
        const fileSize = event.target.files[0].size;
        const imageType = ["image/png", "image/jpeg", "image/gif", "image/bmp"];

        if (fileSize > 2097152) {
            alert("The photo may not be greater than 2 mb.");
            this.clearImage();
            return false;
        }

        if (imageType.indexOf(fileType) === -1) {
            alert("The photo must be a file of type: jpeg, bmp, png, jpg.");
            this.clearImage();
            return false;
        }

        var reader = new FileReader();
        reader.onload = function(ef) {
            this.setState({ photo_preview: ef.target.result });
        }.bind(this);
        reader.readAsDataURL(event.target.files[0]);
        this.setState({
            photo: Array.prototype.slice.call(event.target.files)
        });
    }

    updateDescriptionPhoto(id) {
        this.setState({ editPhoto: id });
    }

    saveDescription(id) {
        const data = {
            description: this.state.descriptionUpdate
        };
        this.props.updatePhoto(this.props.id, id, data);
    }

    render() {
        const {
            modal,
            errors,
            editPhoto,
            descriptionUpdate,
            idUpdate
        } = this.state;
        const { photos } = this.props.photo;

        let descriptionError, messageError, photoError;
        if (!isEmpty(errors)) {
            descriptionError = Boolean(errors.data && errors.data.description);
            photoError = Boolean(errors.data && errors.data.photo);
            console.log("errors", errors.data.photo);
            //messageError = Boolean(errors.data && errors.data.message);
        }

        return (
            <Modal isOpen={modal} toggle={this.close}>
                <Form>
                    <ModalHeader toggle={this.close}>Fotos</ModalHeader>
                    <ModalBody>
                        {isEmpty(photos) && (
                            <Alert color="dark">
                                Você não possue nenhuma foto cadastrada no
                                momento
                            </Alert>
                        )}

                        {!isEmpty(photos) && (
                            // <ul className="list-group">
                            <div>
                                {photos.map(photo => {
                                    return (
                                        <div
                                            className="card mb-3"
                                            key={photo.id}
                                        >
                                            <div className="row no-gutters">
                                                <div className="col-md-4">
                                                    <img
                                                        src={photo.photo_path}
                                                        className="card-img"
                                                    />
                                                </div>
                                                <div className="col-md-8">
                                                    <div className="card-body">
                                                        <p className="card-text">
                                                            {photo.description}
                                                        </p>
                                                        <button
                                                            className="btn btn-secondary btn-sm"
                                                            type="button"
                                                            onClick={this.toggleNested1.bind(
                                                                this,
                                                                photo.id,
                                                                photo.description
                                                            )}
                                                        >
                                                            Alterar
                                                        </button>{" "}
                                                        <button
                                                            className="btn btn-danger btn-sm"
                                                            type="button"
                                                            onClick={this.deletePhoto.bind(
                                                                this,
                                                                photo.id
                                                            )}
                                                        >
                                                            Excluir
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        )}

                        <Modal
                            isOpen={this.state.nestedModal1}
                            toggle={this.toggleNested1.bind(this)}
                        >
                            <ModalHeader>Alterar descrição</ModalHeader>
                            <ModalBody>
                                <FormGroup>
                                    <Label for="descriptionUpdate">
                                        Descrição
                                    </Label>
                                    <Input
                                        type="textarea"
                                        name="descriptionUpdate"
                                        id="descriptionUpdate"
                                        onChange={this.onChange.bind(this)}
                                        invalid={descriptionError}
                                        value={
                                            isEmpty(descriptionUpdate)
                                                ? ""
                                                : descriptionUpdate
                                        }
                                    />
                                </FormGroup>
                            </ModalBody>
                            <ModalFooter>
                                <Button
                                    color="primary"
                                    onClick={this.saveDescription.bind(
                                        this,
                                        idUpdate
                                    )}
                                >
                                    Salvar
                                </Button>{" "}
                                <Button
                                    color="secondary"
                                    onClick={this.toggleNested1.bind(this)}
                                >
                                    Cancelar
                                </Button>
                            </ModalFooter>
                        </Modal>

                        <Modal
                            isOpen={this.state.nestedModal}
                            toggle={this.toggleNested.bind(this)}
                            onClosed={
                                this.state.closeAll ? this.toggle : undefined
                            }
                        >
                            <ModalHeader>Enviar foto</ModalHeader>
                            <ModalBody>
                                {messageError && (
                                    <Alert color="danger">
                                        {errors.data.message}
                                    </Alert>
                                )}
                                <FormGroup>
                                    <Label for="description">Descrição</Label>
                                    <Input
                                        type="textarea"
                                        name="description"
                                        id="description"
                                        onChange={this.onChange.bind(this)}
                                        invalid={descriptionError}
                                    />
                                    {descriptionError && (
                                        <FormFeedback>
                                            {errors.data.description}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label for="photo">Foto</Label>
                                    <Input
                                        type="file"
                                        name="photo"
                                        id="photo"
                                        onChange={this.onChangePhoto.bind(this)}
                                        invalid={photoError}
                                    />
                                    {photoError && (
                                        <FormFeedback>
                                            {errors.data.photo}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                                {this.state.photo_preview && (
                                    <img
                                        src={this.state.photo_preview}
                                        alt="Preview"
                                        className="img-thumbnail"
                                    />
                                )}
                            </ModalBody>
                            <ModalFooter>
                                <Button
                                    color="primary"
                                    onClick={this.savePhoto.bind(this)}
                                >
                                    Salvar
                                </Button>{" "}
                                <Button
                                    color="secondary"
                                    onClick={this.toggleNested.bind(this)}
                                >
                                    Cancelar
                                </Button>
                            </ModalFooter>
                        </Modal>
                    </ModalBody>
                    <ModalFooter>
                        {/* {isEmpty(photos) && ( */}
                            <Button
                                color="primary"
                                onClick={this.newPhoto.bind(this)}
                            >
                                Enviar Fotos
                            </Button>
                        {/* )}{" "} */}
                        <Button color="secondary" onClick={this.close}>
                            Fechar
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

PhotosModal.propTypes = {
    getPhotos: PropTypes.func.isRequired,
    deletePhoto: PropTypes.func.isRequired,
    savePhoto: PropTypes.func.isRequired,
    updatePhoto: PropTypes.func.isRequired,
    photo: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    photo: state.photo,
    message: state.message,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { getPhotos, deletePhoto, savePhoto, updatePhoto }
)(PhotosModal);
