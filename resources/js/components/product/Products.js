import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { deleteProduct, getByCategory } from "../../actions/productActions";
import { Link, Router } from "@reach/router";
import EditProduct from "./EditProduct";
import PhotosModal from "./../photos/PhotosModal";

class Products extends Component {
    componentDidMount() {
        const id = this.props.categoryId;
        this.props.getByCategory(id);
    }

    componentWillReceiveProps(prevProps) {
        if (this.props.categoryId !== prevProps.categoryId) {
            this.props.getByCategory(prevProps.categoryId);
        }
    }

    deleteProduct(id) {
        if (window.confirm("Você tem certeza que deseja excluir?")) {
            this.props.deleteProduct(id);
        }
    }
    render() {
        const { products } = this.props.product;
        return (
            <div>
                <Router>
                    <EditProduct path=":id/edit-product" />
                    <PhotosModal path=":id/photos" />
                </Router>
                <table className="table table-striped mt-3">
                    <thead>
                        <tr>
                            <th
                                scope="col"
                                style={{ width: "100px" }}
                                className="text-center"
                            >
                                C&oacute;digo
                            </th>
                            <th scope="col">Produto</th>
                            <th
                                scope="col"
                                style={{ width: "100px" }}
                                className="text-center"
                            >
                                Ordem
                            </th>
                            <th
                                scope="col"
                                style={{ width: "150px" }}
                                className="text-center"
                            >
                                Op&ccedil;&otilde;es
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map(product => {
                            return (
                                <tr key={product.id}>
                                    <th scope="row" className="text-center">
                                        {product.id}
                                    </th>
                                    <td>{product.name}</td>
                                    <td className="text-center">
                                        {product.order}
                                    </td>

                                    <td
                                        className="text-center"
                                        style={{ whiteSpace: "nowrap" }}
                                    >
                                        <Link
                                            to={`${product.id}/photos`}
                                            className="btn btn-sm btn-outline-dark"
                                        >
                                            Fotos
                                        </Link>{" "}
                                        <Link
                                            to={`${product.id}/edit-product`}
                                            className="btn btn-sm btn-outline-dark"
                                        >
                                            Alterar
                                        </Link>{" "}
                                        <button
                                            type="button"
                                            className="btn btn-sm btn-outline-dark"
                                            onClick={this.deleteProduct.bind(
                                                this,
                                                product.id
                                            )}
                                        >
                                            Excluir
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

Products.propTypes = {
    deleteProduct: PropTypes.func.isRequired,
    getByCategory: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    product: state.product
});

export default connect(
    mapStateToProps,
    { deleteProduct, getByCategory }
)(Products);
