import React, { Component } from "react";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    Input,
    FormFeedback,
    Alert
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import isEmpty from "../../utils/isEmpty";
import { editProduct, getProduct } from "../../actions/productActions";
import { getCategory } from "../../actions/categoryActions";

class AddProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            name: "",
            description: "",
            technical_description: "",
            link_3d: "",
            category_id: "",
            order: "",
            modal: true,
            errors: {},
            saved: false,
            photo: "",
            photo_preview: ""

        };

        this.onChange = this.onChange.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    componentDidMount() {
        this.props.getProduct(this.props.id);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    save(e) {
        e.preventDefault();
        // const data = {
        //     name: this.state.name,
        //     description: this.state.description,
        //     technical_description: this.state.technical_description,
        //     link_3d: this.state.link_3d,
        //     category_id: this.state.category_id,
        //     order: this.state.order
        // };

        const {
            id,
            name,
            description,
            technical_description,
            link_3d,
            order,
            photo,
            category_id
        } = this.state;

        var formData = new FormData();
        if (photo) {
            formData.append("photo", photo[0]);
        }
        formData.append("description", description);
        formData.append("name", name);
        formData.append("order", order);
        formData.append("technical_description", technical_description);
        formData.append("link_3d", link_3d);
        formData.append("category_id", category_id);

        this.props.editProduct(id, formData);
    }

    componentDidUpdate(newProps, a) {
        if (!isEmpty(this.props.product.product)) {
            if (this.props.product.product.id !== a.id || isEmpty(a.id)) {
                const product = this.props.product.product;
                this.setState({
                    id: product.id,
                    name: product.name,
                    description: product.description,
                    technical_description: product.technical_description,
                    link_3d: product.link_3d,
                    category_id: product.category_id,
                    order: product.order,
                    photo_preview: product.photo_path
                });
            }
        }
    }

    componentWillReceiveProps(newProps) {
        if (!isEmpty(newProps.errors)) {
            this.setState({ errors: newProps.errors });
        } else {
            this.setState({ errors: {} });
        }
        if (this.props.product.saved) {
            alert("Salvo com sucesso");
            this.onClose();
        }
    }

    onClose() {
        this.setState({
            modal: false
        });
        setTimeout(function() {
            window.history.back();
        }, 500);
    }

    clearImage() {
        this.setState({ photo: [] });
    }

    onChangePhoto(event) {
        if (event.target.files[0] === undefined) {
            this.clearImage();
            return false;
        }

        const fileType = event.target.files[0].type;
        const fileSize = event.target.files[0].size;
        const imageType = ["image/png", "image/jpeg", "image/gif", "image/bmp"];

        if (fileSize > 2097152) {
            alert("The photo may not be greater than 2 mb.");
            this.clearImage();
            return false;
        }

        if (imageType.indexOf(fileType) === -1) {
            alert("The photo must be a file of type: jpeg, bmp, png, jpg.");
            this.clearImage();
            return false;
        }

        var reader = new FileReader();
        reader.onload = function(ef) {
            this.setState({ photo_preview: ef.target.result });
        }.bind(this);
        reader.readAsDataURL(event.target.files[0]);
        this.setState({
            photo: Array.prototype.slice.call(event.target.files)
        });
    }

    render() {
        const errors = this.state.errors;

        const {
            name,
            description,
            link_3d,
            order,
            technical_description
        } = this.state;

        let nameError, descriptionError, link3DError, orderError, messageError, photoError;
        if (!isEmpty(errors)) {
            nameError = Boolean(
                errors.data && errors.data.data && errors.data.data.name
            );
            descriptionError = Boolean(
                errors.data && errors.data.data && errors.data.data.description
            );
            link3DError = Boolean(
                errors.data && errors.data.data && errors.data.data.link_3d
            );
            orderError = Boolean(
                errors.data && errors.data.data && errors.data.data.order
            );
            photoError = Boolean(
                errors.data && errors.data.data && errors.data.data.photoError
            );
            messageError = Boolean(errors.data && errors.data.message);
        }

        return (
            <Modal
                isOpen={this.state.modal}
                toggle={this.onClose}
            >
                <Form>
                    <ModalHeader toggle={this.onClose}>
                        Alterar Produto
                    </ModalHeader>
                    <ModalBody>
                        {messageError && (
                            <Alert color="danger">{errors.data.message}</Alert>
                        )}
                        <FormGroup>
                            <Label for="name">Nome Produto</Label>
                            <Input
                                type="text"
                                name="name"
                                id="name"
                                onChange={this.onChange}
                                invalid={nameError}
                                value={isEmpty(name) ? "" : name}
                            />
                            {nameError && (
                                <FormFeedback>
                                    {errors.data.data.name}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">
                                Descri&ccedil;&atilde;o
                            </Label>
                            <Input
                                type="textarea"
                                name="description"
                                id="description"
                                onChange={this.onChange}
                                invalid={descriptionError}
                                value={
                                    isEmpty(description)
                                        ? ""
                                        : this.state.description
                                }
                            />
                            {descriptionError && (
                                <FormFeedback>
                                    {errors.data.data.description}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="technical_description">
                                Descri&ccedil;&atilde;o T&eacute;cnica
                            </Label>
                            <Input
                                type="textarea"
                                name="technical_description"
                                id="technical_description"
                                onChange={this.onChange}
                                value={
                                    isEmpty(technical_description)
                                        ? ""
                                        : this.state.technical_description
                                }
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="link_3d">Link 3D</Label>
                            <Input
                                type="text"
                                name="link_3d"
                                id="link_3d"
                                onChange={this.onChange}
                                invalid={link3DError}
                                value={isEmpty(link_3d) ? "" : link_3d}
                            />
                            {link3DError && (
                                <FormFeedback>
                                    {errors.data.data.link_3d}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="photo">Foto</Label>
                            <Input
                                type="file"
                                name="photo"
                                id="photo"
                                onChange={this.onChangePhoto.bind(this)}
                                invalid={photoError}
                            />
                            {photoError && (
                                <FormFeedback>
                                    {errors.data.data.photo}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        {this.state.photo_preview && (
                            <img
                                src={this.state.photo_preview}
                                alt="Preview"
                                className="img-thumbnail"
                            />
                        )}
                        <FormGroup>
                            <Label for="order">Ordem</Label>
                            <Input
                                type="text"
                                name="order"
                                id="order"
                                onChange={this.onChange}
                                invalid={orderError}
                                value={isEmpty(order) ? "" : order}
                            />
                            {orderError && (
                                <FormFeedback>
                                    {errors.data.data.order}
                                </FormFeedback>
                            )}
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button
                            type="submit"
                            color="primary"
                            onClick={this.save.bind(this)}
                        >
                            Save
                        </Button>{" "}
                        <Button color="secondary" onClick={this.onClose}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

AddProduct.propTypes = {
    editProduct: PropTypes.func.isRequired,
    getProduct: PropTypes.func.isRequired,
    getCategory: PropTypes.func.isRequired,
    className: PropTypes.string,
    errors: PropTypes.object,
    product: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    errors: state.errors,
    product: state.product,
    category: state.category
});

export default connect(mapStateToProps, {
    editProduct,
    getProduct,
    getCategory
})(AddProduct);
