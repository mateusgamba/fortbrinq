import React, { Component, Fragment } from "react";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    Input,
    FormFeedback,
    Alert
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import isEmpty from "../../utils/isEmpty";
import { saveCategory, updateCategory } from "../../actions/categoryActions";
import { classnames } from "classnames";

class AddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            image: "",
            order: "",
            modal: false,
            errors: {},
            photo_preview: "",
        };

        this.onChange = this.onChange.bind(this);
        this.onToggleModal = this.onToggleModal.bind(this);

        this.id = props.id ? props.id : undefined;
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    save(e) {
        e.preventDefault();
        const { name, description, image, order } = this.state

        var formData = new FormData();
        if (image) {
            formData.append("image", image[0]);
        }
        formData.append("description", description);
        formData.append("name", name);
        formData.append("order", order);

        if (this.id) {
            this.props.updateCategory(this.id, formData);
        } else {
            this.props.saveCategory(formData);
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.errors) {
            this.setState({ errors: newProps.errors });
        }
        if (this.id) {
            const { name, description, order, image_path, image } = newProps.category.category

            this.setState({
                name,
                description,
                order: order.toString(),
                photo_preview: image ? image_path : '',
            });
        }

        if (newProps && newProps.category && newProps.category.saved) {
            alert("Salvo com sucesso.");
            this.onToggleModal();
        }
    }

    componentDidMount() {
        if (this.id) {
            const { name, description, order, image_path, image } = this.props.category.category

            this.setState({
                name,
                description,
                order: order.toString(),
                photo_preview: image ? image_path : '',
            });
        }
    }

    onToggleModal() {
        this.props.modal();
    }

    clearImage() {
        this.setState({ photo: [] });
    }

    onChangePhoto(event) {
        if (event.target.files[0] === undefined) {
            this.clearImage();
            return false;
        }

        const fileType = event.target.files[0].type;
        const fileSize = event.target.files[0].size;
        const imageType = ["image/png", "image/jpeg", "image/gif", "image/bmp"];

        if (fileSize > 2097152) {
            alert("The photo may not be greater than 2 mb.");
            this.clearImage();
            return false;
        }

        if (imageType.indexOf(fileType) === -1) {
            alert("The photo must be a file of type: jpeg, bmp, png, jpg.");
            this.clearImage();
            return false;
        }

        var reader = new FileReader();
        reader.onload = function(ef) {
            this.setState({ photo_preview: ef.target.result });
        }.bind(this);
        reader.readAsDataURL(event.target.files[0]);
        this.setState({
            image: Array.prototype.slice.call(event.target.files)
        });
    }

    render() {
        const errors = this.state.errors;

        let nameError, descriptionError, imageError, orderError, messageError;

        if (!isEmpty(errors)) {
            nameError = Boolean(
                errors.data && errors.data.data && errors.data.data.name
            );
            descriptionError = Boolean(
                errors.data && errors.data.data && errors.data.data.description
            );
            imageError = Boolean(
                errors.data && errors.data.data && errors.data.data.image
            );
            orderError = Boolean(
                errors.data && errors.data.data && errors.data.data.order
            );
            messageError = Boolean(errors.data && errors.data.message);
        }

        return (
            <Fragment>
                <Modal
                    isOpen={true}
                    toggle={this.onToggleModal}
                >
                    <Form>
                        <ModalHeader toggle={this.onToggleModal}>
                            {this.id ? "Editar" : "Adicionar"} Categoria
                        </ModalHeader>
                        <ModalBody>
                            {messageError && (
                                <Alert color="danger">{errors.data.message}</Alert>
                            )}
                            <FormGroup>
                                <Label for="name">Nome Categoria</Label>
                                <Input
                                    type="text"
                                    name="name"
                                    id="name"
                                    onChange={this.onChange}
                                    invalid={nameError}
                                    value={this.state.name}
                                />
                                {nameError && (
                                    <FormFeedback>
                                        {errors.data.data.name}
                                    </FormFeedback>
                                )}
                            </FormGroup>
                            <FormGroup>
                                <Label for="description">
                                    Descri&ccedil;&atilde;o
                                </Label>
                                <Input
                                    type="textarea"
                                    name="description"
                                    id="description"
                                    onChange={this.onChange}
                                    invalid={descriptionError}
                                    value={this.state.description ? this.state.description : ''}
                                />
                                {descriptionError && (
                                    <FormFeedback>
                                        {errors.data.data.description}
                                    </FormFeedback>
                                )}
                            </FormGroup>
                            <FormGroup>
                                <Label for="image">Foto</Label>
                                <Input
                                    type="file"
                                    name="image"
                                    id="image"
                                    onChange={this.onChangePhoto.bind(this)}
                                    invalid={imageError}
                                />
                                {imageError && (
                                    <FormFeedback>
                                        {errors.data.data.image}
                                    </FormFeedback>
                                )}
                            </FormGroup>
                            {this.state.photo_preview && (
                                <img
                                    src={this.state.photo_preview}
                                    alt="Preview"
                                    className="img-thumbnail"
                                />
                            )}
                            <FormGroup>
                                <Label for="order">Ordem</Label>
                                <Input
                                    type="text"
                                    name="order"
                                    id="order"
                                    onChange={this.onChange}
                                    invalid={orderError}
                                    value={this.state.order}
                                />
                                {orderError && (
                                    <FormFeedback>
                                        {errors.data.data.order}
                                    </FormFeedback>
                                )}
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button
                                type="submit"
                                color="primary"
                                onClick={this.save.bind(this)}
                            >
                                Save
                            </Button>{" "}
                            <Button color="secondary" onClick={this.onToggleModal}>
                                Cancel
                            </Button>
                        </ModalFooter>
                    </Form>
                </Modal>
            </Fragment>
        );
    }
}

AddCategory.propTypes = {
    saveCategory: PropTypes.func.isRequired,
    updateCategory: PropTypes.func.isRequired,
    errors: PropTypes.object,
    category: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    errors: state.errors,
    category: state.category
});

export default connect(mapStateToProps, { saveCategory, updateCategory })(AddCategory);
