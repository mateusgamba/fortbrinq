import React, { Component } from "react";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    Input,
    FormFeedback,
    Alert
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import isEmpty from "../../utils/isEmpty";
import { saveProduct, toggleTab } from "../../actions/productActions";

class AddProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            technical_description: "",
            link_3d: "",
            category_id: "",
            order: "",
            modal: true,
            errors: {},
            photo: "",
            photo_preview: "",
        };

        this.onChange = this.onChange.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    save(e) {
        e.preventDefault();

        const {
            name,
            description,
            technical_description,
            link_3d,
            order,
            photo
        } = this.state;

        var formData = new FormData();
        if (photo) {
            formData.append("photo", photo[0]);
        }
        formData.append("description", description);
        formData.append("name", name);
        formData.append("order", order);
        formData.append("technical_description", technical_description);
        formData.append("link_3d", link_3d);
        formData.append("category_id", this.props.category.category.id);

        this.props.saveProduct(formData, );
    }

    componentWillReceiveProps(newProps) {
        if (newProps.errors) {
            this.setState({ errors: newProps.errors });
        }

        if (!isEmpty(newProps.product.product)) {
            if (isEmpty(this.props.product.product)) {
                alert("Salvo com sucesso.");
                this.onClose();
            } else {
                if (
                    this.props.product.product.id !==
                    newProps.product.product.id
                ) {
                    alert("Salvo com sucesso.");
                    this.onClose();
                }
            }
        }
    }

    onClose() {
        this.setState({ modal: false });
        setTimeout(function() {
            window.history.back();
        }, 500);
    }

    toggle(tab) {
        this.props.toggleTab(tab);
    }

    clearImage() {
        this.setState({ photo: [] });
    }

    onChangePhoto(event) {
        if (event.target.files[0] === undefined) {
            this.clearImage();
            return false;
        }

        const fileType = event.target.files[0].type;
        const fileSize = event.target.files[0].size;
        const imageType = ["image/png", "image/jpeg", "image/gif", "image/bmp"];

        if (fileSize > 2097152) {
            alert("The photo may not be greater than 2 mb.");
            this.clearImage();
            return false;
        }

        if (imageType.indexOf(fileType) === -1) {
            alert("The photo must be a file of type: jpeg, bmp, png, jpg.");
            this.clearImage();
            return false;
        }

        var reader = new FileReader();
        reader.onload = function(ef) {
            this.setState({ photo_preview: ef.target.result });
        }.bind(this);
        reader.readAsDataURL(event.target.files[0]);
        this.setState({
            photo: Array.prototype.slice.call(event.target.files)
        });
    }

    render() {
        const errors = this.state.errors;

        let nameError, descriptionError, link3DError, orderError, messageError, photoError;
        if (!isEmpty(errors)) {
            nameError = Boolean(
                errors.data && errors.data.data && errors.data.data.name
            );
            descriptionError = Boolean(
                errors.data && errors.data.data && errors.data.data.description
            );
            photoError = Boolean(
                errors.data && errors.data.data && errors.data.data.photoError
            );
            link3DError = Boolean(
                errors.data && errors.data.data && errors.data.data.link_3d
            );
            orderError = Boolean(
                errors.data && errors.data.data && errors.data.data.order
            );
            messageError = Boolean(errors.data && errors.data.message);
        }

        return (
            <Modal
                isOpen={this.state.modal}
                toggle={this.onClose}
            >
                <Form>
                    <ModalHeader toggle={this.onClose}>
                        Adicionar Produto
                    </ModalHeader>
                    <ModalBody>
                        {messageError && (
                            <Alert color="danger">{errors.data.message}</Alert>
                        )}
                        <FormGroup>
                            <Label for="name">Nome Produto</Label>
                            <Input
                                type="text"
                                name="name"
                                id="name"
                                onChange={this.onChange}
                                invalid={nameError}
                            />
                            {nameError && (
                                <FormFeedback>
                                    {errors.data.data.name}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">
                                Descri&ccedil;&atilde;o
                            </Label>
                            <Input
                                type="textarea"
                                name="description"
                                id="description"
                                onChange={this.onChange}
                                invalid={descriptionError}
                            />
                            {descriptionError && (
                                <FormFeedback>
                                    {errors.data.data.description}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="technical_description">
                                Descri&ccedil;&atilde;o T&eacute;cnica
                            </Label>
                            <Input
                                type="textarea"
                                name="technical_description"
                                id="technical_description"
                                onChange={this.onChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="link_3d">Link 3D</Label>
                            <Input
                                type="text"
                                name="link_3d"
                                id="link_3d"
                                onChange={this.onChange}
                                invalid={link3DError}
                            />
                            {link3DError && (
                                <FormFeedback>
                                    {errors.data.data.link_3d}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="photo">Foto</Label>
                            <Input
                                type="file"
                                name="photo"
                                id="photo"
                                onChange={this.onChangePhoto.bind(this)}
                                invalid={photoError}
                            />
                            {photoError && (
                                <FormFeedback>
                                    {errors.data.data.photo}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        {this.state.photo_preview && (
                            <img
                                src={this.state.photo_preview}
                                alt="Preview"
                                className="img-thumbnail"
                            />
                        )}
                        <FormGroup>
                            <Label for="order">Ordem</Label>
                            <Input
                                type="text"
                                name="order"
                                id="order"
                                onChange={this.onChange}
                                invalid={orderError}
                            />
                            {orderError && (
                                <FormFeedback>
                                    {errors.data.data.order}
                                </FormFeedback>
                            )}
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button
                            type="submit"
                            color="primary"
                            onClick={this.save.bind(this)}
                        >
                            Save
                        </Button>{" "}
                        <Button color="secondary" onClick={this.onClose}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
        );
    }
}

AddProduct.propTypes = {
    saveProduct: PropTypes.func.isRequired,
    className: PropTypes.string,
    errors: PropTypes.object,
    product: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    errors: state.errors,
    product: state.product,
    category: state.category
});

export default connect(mapStateToProps, { saveProduct, toggleTab })(AddProduct);
