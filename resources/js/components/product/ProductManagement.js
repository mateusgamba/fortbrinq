import React, { Component, Fragment } from "react";
import Categories from "./../category/Categories";
import CategoryProducts from "./CategoryProducts";
import { Router, Link } from "@reach/router";
import PrivateRoute from "./../common/PrivateRoute";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getCategories } from "../../actions/categoryActions";
import NavLink from "./../common/NavLink";
import AddCategory from "./AddCategory";

class ProductManagement extends Component {
    constructor() {
        super();
        this.state = { modal: false };

        this.onToggleModal = this.onToggleModal.bind(this);
    }

    componentDidMount() {
        this.props.getCategories();
    }

    onToggleModal() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    render() {
        const { categories } = this.props.category;
        return (
            <section className="main">
                <div className="container">
                    <div className="row align-items-start">
                        <div className="col-sm-3 mr-2 p-2 rounded bg-white shadow">
                            <h4 className="mb-0">Categorias</h4>
                            <div className="list-group mt-3">
                                {categories.map((category, index) => {
                                    return (
                                        <NavLink
                                            to={`/product-management/${
                                                category.slug
                                            }/products`}
                                            key={`category-${index}`}
                                        >
                                            {category.name}
                                        </NavLink>
                                    );
                                })}
                                <button
                                    type="button"
                                    onClick={this.onToggleModal}
                                    className="list-group-item list-group-item-action list-group-item-dark"
                                >
                                    Adicionar Categoria
                                </button>
                                {this.state.modal && <AddCategory modal={this.onToggleModal} />}
                            </div>
                        </div>

                        <div className="col bg-white rounded bg-white shadow p-2">
                            <Router primary={false}>
                                <CategoryProducts path=":id/products/*" />

                                <PrivateRoute
                                    path="/"
                                    component={CategoryProducts}
                                />
                            </Router>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

ProductManagement.propTypes = {
    getCategories: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    category: state.category
});

export default connect(
    mapStateToProps,
    { getCategories }
)(ProductManagement);
