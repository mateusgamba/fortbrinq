import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import isEmpty from "../../utils/isEmpty";
import { Alert } from "reactstrap";
import Products from "./Products";
import { getCategory } from "../../actions/categoryActions";
import { openAddProductModal } from "../../actions/productActions";
import AddProduct from "./AddProduct";
import ScrollUp from "./../common/ScrollUp";
import { Link, Router } from "@reach/router";
import PrivateRoute from "./../common/PrivateRoute";
import AddCategory from "./AddCategory";

class CategoryProducts extends Component {
    constructor() {
        super();
        this.state = { modal: false };

        this.onToggleModal = this.onToggleModal.bind(this);
        this.excluir = this.excluir.bind(this);
    }

    componentDidMount() {
        const id = this.props.id;
        if (!isEmpty(id)) {
            this.props.getCategory(id);
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.id !== prevProps.id) {
            this.props.getCategory(this.props.id);
        }
    }

    openAddProductModal() {
        this.props.openAddProductModal();
    }

    onToggleModal() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    excluir(id) {
        if (confirm("Tem certeza que deseja Excluir?")) {
            alert("excluido");
        }
        return;
    }

    render() {
        const { category } = this.props.category;

        return (
            <div>
                {!isEmpty(category) ? (
                    <div>
                        <h4 className="mb-0">{category.name}</h4>
                        <Link to="add-photo" className="btn btn-link px-0">
                            Adicionar Produto
                        </Link>
                        {" - "}
                        <button
                            type="button"
                            onClick={this.onToggleModal}
                            className="btn btn-link px-0"
                        >
                            Editar Categoria
                        </button>

                        {/* <button
                            type="button"
                            onClick={() => this.excluir(category.id)}
                            className="btn btn-link px-0"
                        >
                            Excluir Categoria
                        </button> */}
                        {this.state.modal && <AddCategory id={category.id} modal={this.onToggleModal} />}
                        {!isEmpty(category.products) ? (
                            <Products categoryId={category.id} />
                        ) : (
                            <Alert color="warning" className="mt-3">
                                Nenhum produto cadastrado no momento
                            </Alert>
                        )}
                        <Router>
                            <PrivateRoute
                                path="/add-photo"
                                component={AddProduct}
                            />
                            <PrivateRoute
                                path="/add-photo/:id"
                                component={AddProduct}
                            />
                            <PrivateRoute
                                path="/add-photo/:id/photos"
                                component={AddProduct}
                            />
                        </Router>
                    </div>
                ) : (
                    <Alert color="primary">
                        Clique sobre uma das categorias ao lado.
                    </Alert>
                )}
            </div>
        );
    }
}

CategoryProducts.propTypes = {
    category: PropTypes.object.isRequired,
    getCategory: PropTypes.func.isRequired,
    openAddProductModal: PropTypes.func.isRequired,
    product: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    category: state.category,
    product: state.product
});

export default connect(
    mapStateToProps,
    { getCategory, openAddProductModal }
)(CategoryProducts);
