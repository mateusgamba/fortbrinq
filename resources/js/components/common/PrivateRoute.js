import React, { Fragment } from "react";
import { Redirect } from "@reach/router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Header from "../layout/Header";

const PrivateRoute = ({ component: Component, auth, ...rest }) => {
    if (auth.isAuthenticated === false && window.location.hash !== `/`) {
        return <Redirect from="" to="/" />;
    }
    return (
        <Fragment>
            <Header />
            <Component {...rest} />
        </Fragment>
    );
};

PrivateRoute.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(PrivateRoute);
