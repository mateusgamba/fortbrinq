import React from "react";
import { Link } from "@reach/router";

export default props => (
    <Link
        {...props}
        getProps={({ isCurrent }) => {
            return {
                className: isCurrent
                    ? "list-group-item list-group-item-action active"
                    : "list-group-item list-group-item-action"
            };
        }}
    />
);
