# Brinqfort

**1. Clone repository**

```
git clone https://bitbucket.org/magentadigital/brinqfort.git
```

**2. Enter folder**

```
cd fortbrinq
```

**3. Access docker**

```
docker-compose exec app bash
```

**3. Update dependencies from composer**

```
composer update
```

**4. Run Migrations and all database seeds**

```
php artisan migrate:refresh --seed
```

**5. Link public folder**

```
php artisan storage:link
```

Access [http://localhost:9000](http://localhost:9000).

**5. Deploy**

**_5.1. Access SSH_**

```
ssh: ssh -p 65002 u509389106@213.190.6.169
```

```
cd /home/u509389106/domains/fortbrinq.com.br
```

```
rm -rf fortbrinq
```

Remove all public_html files

```
cd public_html
```

```
rm -rf {*,.*}
```

```

ssh -p 22022 netcon10@162.241.2.132

ln -s /home/u509389106/domains/fortbrinq.com.br/fortbrinq/storage/app/public /home/u509389106/domains/fortbrinq.com.br/public_html/storage

ln -s /home3/netcon10/fortbrinq/storage/app/public /home3/netcon10/www.fortbrinq.com.br/storage
ln -s /home3/netcon10/fortbrinq/storage/app/public /home3/netcon10/www.fortbrinq.com.br/public/storage


```

**_Local running_**

```
cd /private/etc
```

```
sudo nano hosts
```

```
213.190.6.189 fortbrinq.com.br
213.190.6.189 www.fortbrinq.com.br
```

```
sudo killall -HUP mDNSResponder; sleep 2
```


#### Updates

```sql

ALTER TABLE `products` ADD COLUMN `photo` VARCHAR(255) NULL AFTER `order`;

update products
set photo=(select photo from product_photos where product_photos.product_id=products.id  and deleted_at is null limit 1)
where 1=1
```

